//
//  Button.swift
//  vPooty
//
//  Created by Alexey Voronov on 31/01/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation
import UIKit

class Button: UIButton {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 8
    }
}
