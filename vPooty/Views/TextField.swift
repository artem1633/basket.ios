//
//  TextField.swift
//  vPooty
//
//  Created by Alexey Voronov on 31/01/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation
import UIKit

class TextField: UITextField {
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.backgroundColor = UIColor(named: "BackgroundGray")?.cgColor
        UIColor.lightGray
        self.layer.cornerRadius = 8
    }
}
