//
//  RoundedCornerView.swift
//  vPooty
//
//  Created by Alexey Voronov on 06/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class RoundedCornerView: UIView {

    override func awakeFromNib() {
        self.layer.cornerRadius = 8
    }

}
