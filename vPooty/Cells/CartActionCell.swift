//
//  CartActionCell.swift
//  vPooty
//
//  Created by Alexey Voronov on 05/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class CartActionCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupCell(totalPrice: String) {
        priceLabel?.text = totalPrice + "р"
    }
    
}
