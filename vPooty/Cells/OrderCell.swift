//
//  OrderCell.swift
//  vPooty
//
//  Created by Alexey Voronov on 07/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(status: String, price: Float, date: String) {
        statusLabel.text = "Статус: " + status
        priceLabel.text = "Сумма: " + String(price) + "р"
        dateLabel.text = "Время заказа: " + date
    }

}
