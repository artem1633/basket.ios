//
//  CategoryNameCell.swift
//  vPooty
//
//  Created by Alexey Voronov on 02/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class CategoryNameCell: UITableViewCell {

    @IBOutlet weak var categoryNameLabel: UILabel?
    
    func setupCell(label: String) {
        categoryNameLabel!.text = label
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
