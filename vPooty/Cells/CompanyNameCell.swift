//
//  CompanyNameCell.swift
//  vPooty
//
//  Created by Alexey Voronov on 02/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class CompanyNameCell: UITableViewCell {
    
    @IBOutlet weak var companyNameLabel: UILabel?
    @IBOutlet weak var companyImage: UIImageView?
    
    func setupCell(label: String, imageURL: URL) {
        companyNameLabel!.text = label
        downloadImage(url: imageURL)
        companyImage?.layer.masksToBounds = true
        companyImage?.image = nil
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print(url)
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.companyImage?.image = UIImage(data: data)!
            }
        }
    }

}
