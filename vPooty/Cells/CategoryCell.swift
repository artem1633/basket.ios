//
//  CategoryCell.swift
//  vPooty
//
//  Created by Alexey Voronov on 01/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
    
    override func didMoveToSuperview() {
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 9
        self.layer.shadowOpacity = 0.12
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func setupCell(categoryName: String, isHighlited: Bool) {
        label.text = categoryName
        if isHighlited {
            self.layer.backgroundColor = UIColor(named: "MainGreen")?.cgColor
            label.textColor = UIColor.white
        } else {
            self.layer.backgroundColor = UIColor.white.cgColor
            label.textColor = UIColor.darkGray
        }
    }
}
