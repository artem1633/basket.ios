//
//  CartProductCellTableViewCell.swift
//  vPooty
//
//  Created by Alexey Voronov on 05/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class CartProductCellTableViewCell: UITableViewCell {

    @IBOutlet weak var countLabel: UILabel?
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var priceLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(count: Int, name: String, price: String) {
        countLabel?.text = String(count) + " x"
        nameLabel?.text = name
        priceLabel?.text = price + "р"
    }

}
