//
//  ProductCell.swift
//  vPooty
//
//  Created by Alexey Voronov on 02/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var productLabel: UILabel?
    @IBOutlet weak var productDescription: UILabel?
    @IBOutlet weak var productPrice: UILabel?
    @IBOutlet weak var productImage: UIImageView?
    
    func setupCell(label: String?, description: String?, price: String?, imageURL: URL?) {
        productLabel!.text = label
        productDescription!.text = description
        productPrice!.text = price
        productImage!.layer.masksToBounds = true
        productImage!.layer.cornerRadius = 8
        productImage!.image = nil
        if imageURL != nil {
            downloadImage(url: imageURL!)
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print(url)
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.productImage?.image = UIImage(data: data)
            }
        }
    }

}
