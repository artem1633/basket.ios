//
//  ApiWorker.swift
//  vPooty
//
//  Created by Alexey Voronov on 31/01/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation
import Alamofire

final class ApiWorker {
    private init() { }
    static let sharedInstance: ApiWorker = ApiWorker()
    
    var tabBar: UITabBarController?
    var name: String = ""
    var sms: String = ""
    var phone: String = ""
    var companies: [Company] = []
    var categories: [Category] = [Category(id: 0, name: "Все")]
    var productCategories: [ProductCategory] = []
    var selectedCompanyId: Int = 0
//   var access_token: String = "ezQX17X3gzao0X1eBFBAUDO-89wGKVDi"
    var access_token: String = ""
    var products: [Product] = []
    var productsInCart: [ProductInCart] = []
    var carInfo: CarInfo?
    var order: Order?
    var orderHistory: [Order] = []
    var productString: String = ""
    var price: Int = 0
    var isTrack = false
    
    let confirmUrl: URL = URL(string: "http://basket.teo-crm.ru/api/client/confirm-register")!
    
    func register(name: String, onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/register?phone=\(self.phone)&name=\(name)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!).responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let errors = JSON["errors"] as? String
                onSuccess(errors ?? "")
            case .failure(let error):
                onFail(error.localizedDescription)
            }
        }
    }
    
    func check(phone: String, onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/confirm-register?phone=\(phone)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                onSuccess(error ?? "")
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
    
    func sms(sms: String, onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/confirm-register?phone=\(self.phone)&sms=\(sms)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                self.access_token = JSON["access"] as? String ?? ""
                print(self.access_token)
                UserDefaults.standard.set(self.access_token, forKey: "token")
                onSuccess(error ?? "")
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
    
    func getAllCompany(onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/get-all-company?access=\(access_token)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                let companyList = JSON["companyList"] as? [NSDictionary]
                if companyList == nil {
                    onSuccess(error ?? "")
                } else {
                    self.companies = []
                    for currentCompany in companyList! {
                        let id = Int((currentCompany["id"] as? String)!)
                        let name = currentCompany["name"] as? String
                        let description = currentCompany["description"] as? String
                        let picture = currentCompany["picture"] as? String
                        let coord_x = Double(currentCompany["coord_x"] as! String)
                        let coord_y = Double(currentCompany["coord_y"] as! String)
                        let company = Company(id: id!, name: name!, description: description!, picture: picture!, coord_x: coord_x!, coord_y: coord_y!)
                        self.companies.append(company)
                    }
                    onSuccess(error ?? "")
                }
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
    
    func getCategories(onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/get-category-company?access=\(access_token)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                let companyList = JSON["categoryCompanyList"] as? [NSDictionary]
                if companyList == nil {
                    onSuccess(error ?? "")
                } else {
                    self.categories = [Category(id: 0, name: "Все")]
                    for currentCompany in companyList! {
                        let id = Int((currentCompany["id"] as? String)!)
                        let name = currentCompany["name"] as? String
                        
                        let category = Category(id: id!, name: name!)
                        self.categories.append(category)
                    }
                    onSuccess(error ?? "")
                }
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
    
    func getCompanyByCategory(id: Int, onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/get-company-by-category?access=\(access_token)&category_id=\(id)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                let companyList = JSON["companyList"] as? [NSDictionary]
                if companyList == nil {
                    onSuccess(error ?? "")
                } else {
                    self.companies = []
                    for currentCompany in companyList! {
                        let id = Int((currentCompany["id"] as? String)!)
                        let name = currentCompany["name"] as? String
                        let description = currentCompany["description"] as? String
                        let picture = currentCompany["picture"] as? String
                        let coord_x = Double(currentCompany["coord_x"] as! String)
                        let coord_y = Double(currentCompany["coord_y"] as! String)
                        let company = Company(id: id!, name: name!, description: description!, picture: picture!, coord_x: coord_x!, coord_y: coord_y!)
                        self.companies.append(company)
                    }
                    onSuccess(error ?? "")
                }
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
    
    func getCategoryByCompany(id: Int, onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/get-category-product?access=\(access_token)&company_id=\(id)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                let productCategorysList = JSON["categoryProductList"] as? [NSDictionary]
                if productCategorysList == nil {
                    onSuccess(error ?? "")
                } else {
                    self.productCategories = []
                    for productCategory in productCategorysList! {
                        let id = Int((productCategory["id"] as? String)!)
                        let name = productCategory["name"] as? String
                        
                        let category = ProductCategory(id: id!, name: name!)
                        self.productCategories.append(category)
                    }
                    onSuccess(error ?? "")
                }
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
    
    func getProductsByCompany(companyId: Int, onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/get-all-product-company?access=\(access_token)&company_id=\(companyId)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                let productList = JSON["productList"] as? [NSDictionary]
                if productList == nil {
                    onSuccess(error ?? "")
                } else {
                    self.products = []
                    for product in productList! {
                        let id = Int((product["id"] as? String)!)
                        let name = product["name"] as? String
                        let description = product["description"] as? String
                        let picture = product["picture"] as? String
                        let weight = product["weight"] as? String
                        let category_product_id = Int((product["category_product_id"] as? String)!)
                        let price = product["price"] as? String
                        
                        let newProduct = Product(id: id!, name: name!, description: description!, picture: picture!, weight: weight!, category_product_id: category_product_id!, price: price!)
                        self.products.append(newProduct)
                    }
                    onSuccess(error ?? "")
                }
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
    
    func createOrder(onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        
        productsToString(products: productsInCart)
        let stringUrl = "http://basket.teo-crm.ru/api/client/add-order?access=\(access_token)&company_id=\(selectedCompanyId)&car_type=\(carInfo!.car_type)&car_number=\(carInfo!.car_number)&car_color=\(carInfo!.car_color)&car_model=\(carInfo!.car_model)&productlist=\(productString)"
        print(stringUrl)
        request(stringUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!).responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                if error == nil {
                    let orderFromJson = JSON["order"] as! NSDictionary
                    let order = Order(companyId: nil, status: nil, order_id: orderFromJson["order_id"] as! Int, time_max: orderFromJson["time_max"] as! Int, summa: orderFromJson["summa"] as! Float, order_date: nil)
                    self.order = order
                }
                onSuccess(error ?? "")
            case .failure(let error):
                onFail(error.localizedDescription)
            }
        }
    }
    
    func productsToString(products: [ProductInCart]) {
        var str = "["
        for (index, product) in products.enumerated() {
            str += """
            {"product_id":\(product.product.id),"amount":\(product.count)}
            """
            if index != products.count - 1 {
                str += ","
            }
        }
        str += "]"
        productString = str
    }
    
    func orderStatus(onSuccess: @escaping (String, Int) -> Void, onFail: @escaping (String) -> Void) {
        print("http://basket.teo-crm.ru/api/client/get-order-status?access=\(access_token)&order_id=\(order!.order_id)")
        request("http://basket.teo-crm.ru/api/client/get-order-status?access=\(access_token)&order_id=\(order!.order_id)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!).responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let errors = JSON["errors"] as? String
                let status = JSON["order_status"] as? Int
                onSuccess(errors ?? "", status ?? 0)
            case .failure(let error):
                print(error)
                onFail("Ошибка сети")
            }
        }
    }
    
    func getOrdersHistory(onSuccess: @escaping (String) -> Void, onFail: @escaping (String) -> Void) {
        request("http://basket.teo-crm.ru/api/client/get-orders-history?access=\(access_token)").responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let value):
                let JSON = value as! NSDictionary
                let error = JSON["errors"] as? String
                let productList = JSON["orders"] as? [NSDictionary]
                if productList == nil {
                    onSuccess(error ?? "")
                } else {
                    self.orderHistory = []
                    for orderHistory in productList! {
                        let order_id = Int((orderHistory["id"] as? String)!)
                        let time_max = Int((orderHistory["time_max"] as? String)!)
                        let summa = Float((orderHistory["summa"] as? String)!)
                        let order_date = orderHistory["order_date"] as? String
                        let status = Int((orderHistory["status"] as? String)!)
                        let company_id = Int((orderHistory["company_id"] as? String)!)
                        
                        let newOredr = Order(companyId: company_id, status: status, order_id: order_id!, time_max: time_max!, summa: summa!, order_date: order_date!)
                        self.orderHistory.append(newOredr)
                    }
                    onSuccess(error ?? "")
                }
            case .failure(let error):
                onFail(error.localizedDescription )
            }
        }
    }
}
