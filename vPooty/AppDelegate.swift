//
//  AppDelegate.swift
//  vPooty
//
//  Created by Alexey Voronov on 31/01/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit
import YandexMapKit
import LSCreditCardForm

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupCreditCard()
        YMKMapKit.setApiKey("63865ce5-30b3-4721-84ca-19efd67029d2")
//        UserDefaults.standard.set("", forKey: "token")
        let token: String! = UserDefaults.standard.string(forKey: "token") ?? ""
        let car_model: String! = UserDefaults.standard.string(forKey: "car_model") ?? ""
        let car_number: String! = UserDefaults.standard.string(forKey: "car_number") ?? ""
        let car_color: String! = UserDefaults.standard.string(forKey: "car_color") ?? ""
        let car_type: String! = UserDefaults.standard.string(forKey: "car_type") ?? ""
        
        ApiWorker.sharedInstance.carInfo = CarInfo(car_type: car_type, car_number: car_number, car_color: car_color, car_model: car_model)

       ApiWorker.sharedInstance.access_token = token
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func setupCreditCard() {
        LSCreditCardFormConfig.TextFieldLabels.localization.cvv = "Код CVV"
        LSCreditCardFormConfig.TextFieldLabels.localization.expiryDate = "Дата окончания"
        LSCreditCardFormConfig.TextFieldLabels.localization.name = "Держатель карты"
        LSCreditCardFormConfig.TextFieldLabels.localization.number = "Номер карты"
        LSCreditCardFormConfig.Buttons.localization.done = "Готово"
        LSCreditCardFormConfig.Buttons.localization.next = "Далее"
        LSCreditCardFormConfig.Buttons.localization.previous = "Назад"
        LSCreditCardFormConfig.Buttons.nextColor = UIColor(named: "MainGreen")
        LSCreditCardFormConfig.Buttons.prevColor = UIColor(named: "MainGreen")
    }


}

