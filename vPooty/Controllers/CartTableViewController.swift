//
//  CartTableViewController.swift
//  vPooty
//
//  Created by Alexey Voronov on 05/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit
import BLTNBoard

class CartTableViewController: UITableViewController {
    
    var selectedId = 0
    
    lazy var bulletinManager: BLTNItemManager = {
        let introPage = BLTNPageItem(title: ApiWorker.sharedInstance.productsInCart[selectedId].product.name)
        return BLTNItemManager(rootItem: introPage)
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = .none
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != ApiWorker.sharedInstance.productsInCart.count {
            tapToProduct(id: indexPath.row)
        } else {
            tapOnAction()
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ApiWorker.sharedInstance.productsInCart.count == 0 {
            return 0
        } else {
            return ApiWorker.sharedInstance.productsInCart.count + 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != ApiWorker.sharedInstance.productsInCart.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartProductCell") as! CartProductCellTableViewCell
            print(ApiWorker.sharedInstance.productsInCart.count)
            print(indexPath.row)
            let product = ApiWorker.sharedInstance.productsInCart[indexPath.row]
            cell.setupCell(count: product.count, name: product.product.name, price: product.product.price)
            cell.selectionStyle = .none
            return cell as UITableViewCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartActionCell") as! CartActionCell
            var price = 0
            for product in ApiWorker.sharedInstance.productsInCart {
                price += Int(product.product.price)! * Int(product.count)
            }
            ApiWorker.sharedInstance.price = price
            cell.setupCell(totalPrice: "Итог:     " + String(price))
            cell.selectionStyle = .none
            return cell as UITableViewCell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row != ApiWorker.sharedInstance.productsInCart.count {
            return 44
        } else {
            return 134
        }
    }
    
    func tapToProduct(id: Int) {
        selectedId = id
        let introPage = BLTNPageItem(title: ApiWorker.sharedInstance.productsInCart[id].product.name)
        introPage.appearance.titleTextColor = UIColor.black
        introPage.appearance.titleFontSize = 20
        introPage.appearance.descriptionFontSize = 16
        introPage.appearance.actionButtonColor = UIColor.red
        introPage.descriptionText = ApiWorker.sharedInstance.productsInCart[id].product.description
        introPage.actionButtonTitle = "Удалить из корзины"
        introPage.actionHandler = { item in
            if ApiWorker.sharedInstance.productsInCart[id].count > 1 {
                ApiWorker.sharedInstance.productsInCart[id].count -= 1
            } else {
                ApiWorker.sharedInstance.productsInCart.remove(at: id)
            }
            self.tableView.reloadData()
            self.bulletinManager.dismissBulletin(animated: true)
        }
        self.bulletinManager = BLTNItemManager(rootItem: introPage)
        bulletinManager.showBulletin(in: UIApplication.shared)
    }
    
    func tapOnAction() {
        performSegue(withIdentifier: "AddCarController", sender: nil)
    }

}
