//
//  ViewController.swift
//  vPooty
//
//  Created by Alexey Voronov on 31/01/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class PhoneViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textField: TextField!
    @IBAction func nextAction(sender: UIButton) {
        checkField()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = "+7"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        checkField()
        return true
    }
    
    func checkField() {
        if (textField.text?.count)! == 12 {
            ApiWorker.sharedInstance.phone = textField.text!
            apiRequest()
        } else {
            alert(title: "Ошибка ввода", message: "Введите номер телефона в формате: +79601234567")
        }
    }
    
    func toSmsViewController() {
        performSegue(withIdentifier: "SmsViewController", sender: nil)
    }
    
    func toNameViewController() {
        performSegue(withIdentifier: "NameViewController", sender: nil)
    }
    
    func apiRequest() {
        ApiWorker.sharedInstance.check(phone: textField.text!, onSuccess: { (error) in
            if error == "" {
                self.toSmsViewController()
            } else if error == "Телефон не зарегистрирован" {
                self.toNameViewController()
            } else {
                self.alert(title: "Ошибка", message: error)
            }
        }) { (error) in
            self.alert(title: "Ошибка", message: error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }


}

