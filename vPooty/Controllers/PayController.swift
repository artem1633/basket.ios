//
//  PayControllerViewController.swift
//  vPooty
//
//  Created by Alexey Voronov on 06/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit
import BLTNBoard
import LSCreditCardForm


class PayController: UIViewController, LSCreditCardFormDelegate {
    
    let network = NetworkService()
    
    lazy var bulletinManager: BLTNItemManager = {
        let introPage = BLTNPageItem(title: "Оплата заказа")
        return BLTNItemManager(rootItem: introPage)
    }()
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var viewCCForm: LSCreditCardFormView!
    @IBOutlet weak var ConstraintKeyBoard: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        hideLoadingIndicator()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardNotification),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)

        
        viewCCForm.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let endFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            ConstraintKeyBoard.constant = endFrame.height
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }
    }
    
    func showCard() {
        let introPage = BLTNPageItem(title: "Заказ оплачен")
        introPage.descriptionText = "Примерное время ожидания: " + String(ApiWorker.sharedInstance.order!.time_max) + " минут"
        introPage.actionButtonTitle = "К отслеживанию"
        introPage.image = UIImage(named: "IntroCompletion")
        introPage.appearance.actionButtonColor = UIColor(named: "MainGreen")!
        introPage.appearance.imageViewTintColor = UIColor(named: "MainGreen")!
        introPage.actionHandler = { item in
            self.bulletinManager.dismissBulletin(animated: true)
            self.toTracking()
        }
        
        
        self.bulletinManager = BLTNItemManager(rootItem: introPage)
        bulletinManager.showBulletin(in: UIApplication.shared)
    }
    
    func toTracking() {
        ApiWorker.sharedInstance.productsInCart = []
        ApiWorker.sharedInstance.isTrack = true
        self.navigationController?.popToRootViewController(animated: false)
        ApiWorker.sharedInstance.tabBar!.selectedIndex = 0
    }
    
    func didCompleteForm(creditCard: LSCreditCard) {
        if (creditCard.number.count < 8 || creditCard.expiration.count < 2 || creditCard.cardHolderName.count < 2 || creditCard.cvv.count < 3) {
            alert(title: "Ошибка", message: "Проверьте данные карты")
        } else {
            print(creditCard.number)
            print(creditCard.expiration)
            print(creditCard.cardHolderName)
            print(creditCard.cvv)
            createPayment(cardNumber: creditCard.number, expDate: creditCard.expiration, cvv: creditCard.cvv, holderName: creditCard.cardHolderName)
            self.view.endEditing(true)
        }
    }
    
    func getCustomCardType(for number: String) -> LSCreditCardType? {
        return number.hasPrefix("493428") ? .custom("mada_visa") : nil
    }
    
    func createPayment(cardNumber: String, expDate: String, cvv: String, holderName: String) {
        let card = Card()
        let cardCryptogramPacket = card.makeCryptogramPacket(cardNumber, andExpDate: expDate, andCVV: cvv, andMerchantPublicID: Constants.merchantPulicId)
        charge(cardCryptogramPacket: cardCryptogramPacket!, cardHolderName: "Alexey Voronov")
    }
    
    func charge(cardCryptogramPacket: String, cardHolderName: String) {
        print("----------")
        print("----------")
        print(cardCryptogramPacket)
        print("----------")
        print("----------")
        self.showLoadingIndicator()
        
        network.charge(cardCryptogramPacket: cardCryptogramPacket, cardHolderName: cardHolderName) { [weak self] result in
            
            self?.hideLoadingIndicator()
            
            switch result {
            case .success(let transactionResponse):
                print("success")
                self?.checkTransactionResponse(transactionResponse: transactionResponse)
            case .failure(let error):
                print("error")
                self?.showAlert(title: "Ошибка", message: error.localizedDescription)
            }
        }
    }
    
    func checkTransactionResponse(transactionResponse: TransactionResponse) {
        print(transactionResponse.transaction?.acsUrl)
        if (transactionResponse.success) {
            ApiWorker.sharedInstance.createOrder(onSuccess: { (error) in
                print(error)
                self.showCard()
            }) { (error) in
                print(error)
            }
        } else {
            
            if (!transactionResponse.message.isEmpty) {
                self.showAlert(title: "Ошибка", message: transactionResponse.message)
                return
            }
            if (transactionResponse.transaction?.paReq != nil && transactionResponse.transaction?.acsUrl != nil) {
                
                let transactionId = String(describing: transactionResponse.transaction?.transactionId ?? 0)
                
                // Показываем 3DS форму
                print("Показываем 3DS форму")
                print(transactionResponse.transaction?.acsUrl)
                print(transactionResponse.transaction?.paReq)
                print(transactionId)
                D3DS.make3DSPayment(with: self, andAcsURLString: transactionResponse.transaction?.acsUrl, andPaReqString: transactionResponse.transaction?.paReq, andTransactionIdString: transactionId)
            } else {
                self.showAlert(title: "Ошибка", message: transactionResponse.transaction?.cardHolderMessage)
            }
        }
    }
    
    func post3ds(transactionId: String, paRes: String) {
        
        self.showLoadingIndicator()
        
        network.post3ds(transactionId: transactionId, paRes: paRes) { [weak self] result in
            
            self?.hideLoadingIndicator()
            
            switch result {
            case .success(let transactionResponse):
                print("success")
                self?.checkTransactionResponse(transactionResponse: transactionResponse)
            case .failure(let error):
                print("error")
                self?.showAlert(title: "Ошибка", message: error.localizedDescription)
            }
        }
    }
    
    func showAlert(title: String?, message: String?, actions: [UIAlertAction]? = nil) {
        let alert = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)
        
        var alertActions = actions ?? []
        
        if alertActions.isEmpty {
            alertActions.append(UIAlertAction(title: "OK", style: .default))
        }
        
        alertActions.forEach { alert.addAction($0) }
        
        present(alert, animated: true)
    }
    
    func showLoadingIndicator() {
        self.loadingIndicator.isHidden = false
        self.loadingIndicator.startAnimating()
    }
    
    func hideLoadingIndicator() {
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true
    }

}

extension PayController: UIWebViewDelegate {
    
    func parse(response: String?) -> [AnyHashable: Any]? {
        guard let response = response else {
            return nil
        }
        
        let pairs = response.components(separatedBy: "&")
        let elements = pairs.map { $0.components(separatedBy: "=") }
        let dict = elements.reduce(into: [String: String]()) {
            $0[$1[0].removingPercentEncoding!] = $1[1].removingPercentEncoding
        }
        
        return dict
    }
    
    /// Handle result from 3DS form
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        let urlString = request.url?.absoluteString
        if (urlString == Constants.cloudpaymentsURL) {
            var response: String? = nil
            if let aBody = request.httpBody {
                response = String(data: aBody, encoding: .ascii)
            }
            let responseDictionary = parse(response: response)
            webView.removeFromSuperview()
            post3ds(transactionId: responseDictionary?["MD"] as! String, paRes: responseDictionary?["PaRes"] as! String)
            return false
        }
        return true
    }
}
