//
//  CarInfoController.swift
//  vPooty
//
//  Created by Alexey Voronov on 06/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class CarInfoController: UIViewController {
    
    @IBOutlet weak var markField: UITextField?
    @IBOutlet weak var modelField: UITextField?
    @IBOutlet weak var colorField: UITextField?
    @IBOutlet weak var numberField: UITextField?
    
    @IBAction func NextAction(_ sender: Any) {
        checkFields()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillFields()
    }
    
    func checkFields() {
        if markField?.text == "" || modelField?.text == "" || colorField?.text == "" || numberField?.text == "" {
            alert(title: "Не заполненны поля", message: "Пожалуйста заполните все поля")
        } else {
            let car = CarInfo(car_type: (markField?.text)!, car_number: (numberField?.text)!, car_color: (colorField?.text)!, car_model: (modelField?.text)!)
            ApiWorker.sharedInstance.carInfo = car
            UserDefaults.standard.set(car.car_model, forKey: "car_model")
            UserDefaults.standard.set(car.car_number, forKey: "car_number")
            UserDefaults.standard.set(car.car_color, forKey: "car_color")
            UserDefaults.standard.set(car.car_type, forKey: "car_type")
            toPayController()
            
        }
    }
    

    func toPayController() {
        performSegue(withIdentifier: "PayController", sender: nil)
    }
    
    func fillFields() {
        if let car = ApiWorker.sharedInstance.carInfo {
            markField?.text = car.car_type
            modelField?.text = car.car_model
            colorField?.text = car.car_color
            numberField?.text = car.car_number
        }
    }

}
