//
//  OrdersHistoryTableController.swift
//  vPooty
//
//  Created by Alexey Voronov on 07/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class OrdersHistoryTableController: UITableViewController {

    let statusDic = [0: "Предварительный", 1: "Новый", 2: "В работе", 3: "В доставке", 4: "Выполнен"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOrdersHistory()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ApiWorker.sharedInstance.orderHistory.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        let order = ApiWorker.sharedInstance.orderHistory[indexPath.row]
        cell.setupCell(status: statusDic[order.status!]!, price: order.summa, date: order.order_date!)
        if order.status == 4 {
            cell.accessoryType = .checkmark
        }
        return cell as UITableViewCell
    }
    
    func getOrdersHistory() {
        ApiWorker.sharedInstance.getOrdersHistory(onSuccess: { (error) in
            print(error)
            print(ApiWorker.sharedInstance.orderHistory)
            self.tableView.reloadData()
        }) { (error) in
            self.alert(title: "Ошибка сети", message: error)
            print(error)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let order = ApiWorker.sharedInstance.orderHistory[indexPath.row]
        
        if order.status != 4 {
            ApiWorker.sharedInstance.selectedCompanyId = order.companyId!
            ApiWorker.sharedInstance.order = order
            
            ApiWorker.sharedInstance.isTrack = true
            ApiWorker.sharedInstance.tabBar!.selectedIndex = 0
        }
    }

}
