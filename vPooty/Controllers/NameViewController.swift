//
//  NameViewController.swift
//  vPooty
//
//  Created by Alexey Voronov on 31/01/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit

class NameViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textField: TextField!
    @IBAction func nextAction(sender: UIButton) {
        checkField()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        checkField()
        return true
    }
    
    func checkField() {
        if (textField.text?.count)! > 0 {
            ApiWorker.sharedInstance.name = textField.text!
            apiRequest()
        } else {
            alert(title: "Ошибка ввода", message: "Введите имя")
        }
    }
    
    func toSmsViewController() {
        performSegue(withIdentifier: "SmsViewController", sender: nil)
    }
    
    func apiRequest() {
        ApiWorker.sharedInstance.register(name: textField.text!, onSuccess: { (error) in
            if error == "" {
                self.toSmsViewController()
            } else {
                self.alert(title: "Ошибка", message: error)
            }
        }) { (error) in
            self.alert(title: "Ошибка", message: error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
