//
//  MenuTableViewController.swift
//  vPooty
//
//  Created by Alexey Voronov on 02/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit
import BLTNBoard



class MenuTableViewController: UITableViewController {
    
    lazy var bulletinManager: BLTNItemManager = {
        let introPage = BLTNPageItem(title: products[selectedId].name)
        return BLTNItemManager(rootItem: introPage)
    }()
    
    
    struct TypeOfCell {
        let type: Int
        let id: Int
    }
    
    var selectedCompanyId: Int = 0
    var selectedId: Int = 0
    var productCategorys: [ProductCategory] = []
    var products: [Product] = []
    var typeOfCells: [TypeOfCell] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        self.tableView.separatorStyle = .none
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(toMapViewController))
        swipeRecognizer.direction = .right
        self.view.addGestureRecognizer(swipeRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if selectedCompanyId != ApiWorker.sharedInstance.selectedCompanyId {
            products = []
            ApiWorker.sharedInstance.productsInCart = []
            self.setCartBadgeCount(count: 0)
            self.tableView.reloadData()
            getCtegoriesByCompany(id: ApiWorker.sharedInstance.selectedCompanyId)
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row != 0 {
            if typeOfCells[indexPath.row - 1].type == 1 {
                tapToProduct(id: typeOfCells[indexPath.row - 1].id)
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if products.count == 0 {
            return 0
        } else {
            return products.count + 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowId = indexPath.row - 1
        if indexPath.row == 0 {
            let companies = ApiWorker.sharedInstance.companies
            let company = companies.first(where: {$0.id == ApiWorker.sharedInstance.selectedCompanyId})
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyNameCell") as! CompanyNameCell
            cell.selectionStyle = .none
            cell.setupCell(label: company!.name, imageURL: URL(string: "http:/basket.teo-crm.ru" +  company!.picture)!)
            return cell as UITableViewCell
        } else if typeOfCells[rowId].type == 1{
            let productId = typeOfCells[rowId].id
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell") as! ProductCell
            cell.selectionStyle = .none
            cell.setupCell(label: products[productId].name, description: products[productId].description, price: products[productId].price + "р", imageURL: URL(string: "http:/basket.teo-crm.ru" +  products[productId].picture))
            return cell as UITableViewCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryNameCell") as! CategoryNameCell
            cell.selectionStyle = .none
            cell.setupCell(label: productCategorys[typeOfCells[rowId].id].name)
            return cell as UITableViewCell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowId = indexPath.row - 1
        if indexPath.row == 0 {
            return 245
        } else if typeOfCells[rowId].type == 1 {
            return 250
        } else {
            return 70
        }
    }
    
    
    
    func getProductsByCompany (id: Int) {
        ApiWorker.sharedInstance.getProductsByCompany(companyId: id, onSuccess: { (error) in
            print(error)
            self.products = ApiWorker.sharedInstance.products
            self.tableView.reloadData()
            self.createTypeOfCells()
        }) { (error) in
            self.alert(title: "Ошибка сети", message: error)
        }
    }
    
    func getCtegoriesByCompany (id: Int) {
        ApiWorker.sharedInstance.getCategoryByCompany(id: id, onSuccess: { (error) in
            print(error)
            self.selectedCompanyId = ApiWorker.sharedInstance.selectedCompanyId
            self.getProductsByCompany(id: ApiWorker.sharedInstance.selectedCompanyId)
            self.productCategorys = ApiWorker.sharedInstance.productCategories
        }) { (error) in
            self.alert(title: "Ошибка сети", message: error)
            print(error)
        }
    }
    
    func createTypeOfCells() {
        typeOfCells = []
        for (categoryIndex, category) in productCategorys.enumerated() {
            typeOfCells.append(TypeOfCell(type: 0, id: categoryIndex))
            var categoryProducts: [Product] = []
            for product in products {
                if category.id == product.category_product_id {
                    categoryProducts.append(product)
                }
            }
            for (productIndex, _) in categoryProducts.enumerated() {
                typeOfCells.append(TypeOfCell(type: 1, id: productIndex))
            }
        }
    }
    
    func tapToProduct(id: Int) {
        print(products[selectedId].name)
        self.selectedId = id
        let introPage = BLTNPageItem(title: products[selectedId].name)
        introPage.appearance.titleTextColor = UIColor.black
        introPage.appearance.titleFontSize = 20
        introPage.appearance.descriptionFontSize = 16
        introPage.appearance.actionButtonColor = UIColor(named: "MainGreen")!
        introPage.appearance.alternativeButtonTitleColor = UIColor(named: "MainGreen")!
        introPage.descriptionText = products[selectedId].description
        introPage.actionButtonTitle = "Добавить в корзину" + "  |  "+products[selectedId].price+" руб"
        introPage.alternativeButtonTitle = "Перейти в корзину"
        introPage.actionHandler = { item in
            let product = self.products[self.selectedId]
            if let productsInCartIndex = ApiWorker.sharedInstance.productsInCart.firstIndex(where: {$0.product.id == product.id}) {
                ApiWorker.sharedInstance.productsInCart[productsInCartIndex].count += 1
            } else {
                ApiWorker.sharedInstance.productsInCart.append(ProductInCart(count: 1, product: self.products[self.selectedId]))
            }
            var count: Int = 0
            for product in ApiWorker.sharedInstance.productsInCart {
                count += product.count
            }
            self.setCartBadgeCount(count: count)
            self.bulletinManager.dismissBulletin(animated: true)
        }
        introPage.alternativeHandler = { item in
            let tabBar = self.tabBarController
            tabBar!.selectedIndex = 2
        }
        self.bulletinManager = BLTNItemManager(rootItem: introPage)
        bulletinManager.showBulletin(in: UIApplication.shared)
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    @objc func toMapViewController() {
        let tabBar = self.tabBarController
        tabBar!.selectedIndex = 0
    }
    
    func setCartBadgeCount(count: Int) {
        let item = self.tabBarController!.tabBar.items![2] as! UITabBarItem
        item.badgeColor = UIColor(named: "MainGreen")
        item.badgeValue = String(count)
    }

}
