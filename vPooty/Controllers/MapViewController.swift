//
//  MapViewController.swift
//  vPooty
//
//  Created by Alexey Voronov on 31/01/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import UIKit
import YandexMapKit
import YandexMapKitDirections
import CoreLocation

class MapViewController: UIViewController, YMKUserLocationObjectListener, YMKMapObjectTapListener, UICollectionViewDelegate, UICollectionViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var mapView: YMKMapView!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyDescription: UILabel!
    @IBOutlet weak var companyDistance: UILabel!
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoriesCollection: UICollectionView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBAction func closeButtonAction(_ sender: Any) {
        closeCompanyView(completion: {})
    }
    @IBAction func menuButtonAction(_ sender: Any) {
        toMenuTableViewController()
    }
    @IBAction func navigationAction(_ sender: Any) {
        let company = ApiWorker.sharedInstance.companies.first(where: {$0.id == ApiWorker.sharedInstance.selectedCompanyId})!
        let url = URL(string: "yandexnavi://build_route_on_map?lat_to=\(company.coord_x)&lon_to=\(company.coord_y)")
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
    }
    
    var time: Timer?
    
    let statusDic = [0: "Предварительный", 1: "Новый", 2: "В работе", 3: "В доставке", 4: "Выполнен"]
    
    var locationManager = CLLocationManager()
    
    var drivingSession: YMKDrivingSession?
    
    var isCompanyViewOpened = false
    
    var mapObjectsArr: [YMKPlacemarkMapObject] = []
    
    var selectedCollectionId: Int = 0
    
    var selectedCompanyId: Int = 0
    
    var myLocation: CLLocation?
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        myLocation = location
        let target = YMKPoint(latitude:  location.coordinate.latitude, longitude: location.coordinate.longitude)
        let position = YMKCameraPosition.init(target: target, zoom: 10, azimuth: 0, tilt: 0)
        let animation = YMKAnimation(type: YMKAnimationType.smooth, duration: 0)
        
        addRout(location: location)
        
        mapView.mapWindow.map.move(with: position, animationType: animation, cameraCallback: nil)
        locationManager.stopUpdatingLocation()
        
        let userlocation = mapView.mapWindow.map.userLocationLayer
        
        userlocation.resetAnchor()
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        print("locationManagerDidResumeLocationUpdates")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        print("didUpdateHeading")
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        print("pause")
    }
    
    func locationManagerShouldDisplayHeadingCalibration(_ manager: CLLocationManager) -> Bool {
        print("locationManagerShouldDisplayHeadingCalibration")
        return false
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if ApiWorker.sharedInstance.isTrack == true {
            menuButton.isHidden = true
        } else {
            menuButton.isHidden = false
        }
        
            
        locationManager.startUpdatingLocation()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            
        }
        
        decorateCompanyView()
        
        getAllCompany()
        
        mapSetup()
        
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCollectionId = indexPath.row
        if indexPath.row == 0{
            getAllCompany()
        } else {
            getCompanyByCategory(id: ApiWorker.sharedInstance.categories[indexPath.row].id)
        }
        categoriesCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ApiWorker.sharedInstance.isTrack == false {
            return ApiWorker.sharedInstance.categories.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.setupCell(categoryName: ApiWorker.sharedInstance.categories[indexPath.row].name, isHighlited: indexPath.row == selectedCollectionId)
        print(selectedCollectionId)
        return cell as UICollectionViewCell
    }
    
    func closeCompanyView(completion: @escaping () -> Void) {
        if ApiWorker.sharedInstance.isTrack == false {
            self.isCompanyViewOpened = false
            self.companyViewBottomConstraint.constant = 200
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: [], animations: {
                self.view.layoutIfNeeded()
                }, completion: {(finished: Bool) in
                    completion()
            })
        } else {
            self.time = nil
            ApiWorker.sharedInstance.isTrack = false
            self.isCompanyViewOpened = false
            self.companyViewBottomConstraint.constant = 200
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: [], animations: {
                self.view.layoutIfNeeded()
            }, completion: {(finished: Bool) in
                completion()
                self.viewWillAppear(false)
            })
        }
    }
    
    func openCompanyView() {
        companyImage.image = nil
        isCompanyViewOpened = true
        if ApiWorker.sharedInstance.isTrack == false {
            companyViewBottomConstraint.constant = 66
        } else {
            companyViewBottomConstraint.constant = 0
        }
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: [], animations: {
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func showCompany(companyId: Int) {
        if isCompanyViewOpened == true {
            self.closeCompanyView {
                self.setupCompanyView(companyId: companyId)
                self.openCompanyView()
            }
        } else {
            self.setupCompanyView(companyId: companyId)
            self.openCompanyView()
        }
        
    }
    
    func setupCompanyView(companyId: Int) {
        let companies = ApiWorker.sharedInstance.companies
        let company = companies.first(where: {$0.id == companyId})
        companyName.text = company?.name
        companyDescription.text = company?.description
        let distance = self.distance(firstPoint: myLocation!, secondPoint: CLLocation(latitude: (company?.coord_x)!, longitude: (company?.coord_y)!))
        
        companyDistance.text = String(format: "%.01f км", distance)
        downloadImage(url: URL(string: "http:/basket.teo-crm.ru" + company!.picture)!)
        
    }
    
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        if ApiWorker.sharedInstance.isTrack == false {
            let id = mapObject.userData as! Int
            selectedCompanyId = id
            showCompany(companyId: id)
            for obj in mapObjectsArr {
                let placemark = obj
                placemark.setIconStyleWith(YMKIconStyle(
                    anchor: CGPoint(x: 0.5, y: 0.5) as NSValue,
                    rotationType:YMKRotationType.rotate.rawValue as NSNumber,
                    zIndex: 0,
                    flat: true,
                    visible: true,
                    scale: 1,
                    tappableArea: nil))
            }
            let pin = mapObject as! YMKPlacemarkMapObject
            pin.setIconStyleWith(YMKIconStyle(
                anchor: CGPoint(x: 0.5, y: 0.5) as NSValue,
                rotationType:YMKRotationType.rotate.rawValue as NSNumber,
                zIndex: 0,
                flat: true,
                visible: true,
                scale: 1.3,
                tappableArea: nil))
            ApiWorker.sharedInstance.selectedCompanyId = selectedCompanyId
        }
        return true
    }
    
    func onObjectAdded(with view: YMKUserLocationView) {
        view.arrow.setIconWith(UIImage(named: "pin")!)
        let pinPlacemark = view.pin.useCompositeIcon()
        pinPlacemark.setIconWithName("pin",
                                     image: UIImage(named:"pin")!,
                                     style:YMKIconStyle(
                                        anchor: CGPoint(x: 0.5, y: 0.5) as NSValue,
                                        rotationType:YMKRotationType.rotate.rawValue as NSNumber,
                                        zIndex: 0,
                                        flat: true,
                                        visible: true,
                                        scale: 1,
                                        tappableArea: nil))
        view.accuracyCircle.fillColor = UIColor(red: 0, green: 1, blue: 0, alpha: 1)
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {
    }
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {
        print("onObjectUpdated")
    }
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        ApiWorker.sharedInstance.tabBar = self.tabBarController
        
        let mapObjects = mapView.mapWindow.map.mapObjects
        mapObjects.addTapListener(with: self)
        
        categoriesCollection.delegate = self
        categoriesCollection.dataSource = self
        categoriesCollection.layer.masksToBounds = false
    }
    
    func getAllCompany() {
        ApiWorker.sharedInstance.getAllCompany(onSuccess: { (error) in
            if error == "В системе нет такого ключа" {
                self.toPhoneViewController()
            } else {
                if ApiWorker.sharedInstance.isTrack == true {
                    self.priceLabel.text = "Сумма: " + String(ApiWorker.sharedInstance.order!.summa) + "р"
                    self.setupCompanyView(companyId: ApiWorker.sharedInstance.selectedCompanyId)
                    self.openCompanyView()
                    self.getOrderStatus()
                    if self.time == nil {
                        self.time = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.getOrderStatus), userInfo: nil, repeats: true)
                    }
                    
                }
                self.addObjectsToMap()
                self.getCategories()
            }
        }, onFail: { (error) in
            self.alert(title: "Ошибка сети", message: error)
        })
    }
    
    func getCompanyByCategory(id: Int) {
        ApiWorker.sharedInstance.getCompanyByCategory(id: id,onSuccess: { (error) in
            if error == "В системе нет такого ключа" {
                self.toPhoneViewController()
            } else {
                self.addObjectsToMap()
            }
        }, onFail: { (error) in
            self.alert(title: "Ошибка сети", message: error)
        })
    }
    
    func getCategories() {
        ApiWorker.sharedInstance.getCategories(onSuccess: { (error) in
            if error == "В системе нет такого ключа" {
                self.toPhoneViewController()
            } else {
                self.categoriesCollection.reloadData()
            }
        }, onFail: { (error) in
            self.alert(title: "Ошибка сети", message: error)
        })
    }
    
    func decorateCompanyView() {
        companyImage.layer.cornerRadius = 8.0
        companyImage.clipsToBounds = true
        companyView.layer.cornerRadius = 12
        companyView.layer.shadowColor = UIColor.black.cgColor
        companyView.layer.shadowOpacity = 0.12
        companyView.layer.shadowOffset = CGSize.zero
        companyView.layer.shadowRadius = 10
    }
    
    
    func mapSetup() {
        mapView.mapWindow.map.isRotateGesturesEnabled = false
        if ApiWorker.sharedInstance.isTrack == false {
 //           let target = YMKPoint(latitude: 0, longitude: 0)
 //           mapView.mapWindow.map.move(with:
 //               YMKCameraPosition(target: target ,zoom: 10, azimuth: 0, tilt: 0))
        } else {
            let company = ApiWorker.sharedInstance.companies.first(where: {$0.id == ApiWorker.sharedInstance.selectedCompanyId})!
            let target = YMKPoint(latitude: (company.coord_x), longitude: (company.coord_y))
            mapView.mapWindow.map.move(with:
                YMKCameraPosition(target: target ,zoom: 10, azimuth: 0, tilt: 0))
        }
        let scale = UIScreen.main.scale
        let userLocationLayer = mapView.mapWindow.map.userLocationLayer
        
        userLocationLayer.isEnabled = true
        userLocationLayer.isHeadingEnabled = false
        userLocationLayer.setAnchorWithAnchorNormal(
            CGPoint(x: 0.5 * mapView.frame.size.width * scale, y: 0.5 * mapView.frame.size.height * scale),
            anchorCourse: CGPoint(x: 0.5 * mapView.frame.size.width * scale, y: 0.83 * mapView.frame.size.height * scale))
        userLocationLayer.setObjectListenerWith(self)
    }
    
    func addObjectsToMap() {
        mapView.reloadInputViews()
        
        let mapObjects = mapView.mapWindow.map.mapObjects
        mapObjects.clear()
        mapObjectsArr = []
        
        if ApiWorker.sharedInstance.isTrack == false {
        
            let companies = ApiWorker.sharedInstance.companies
            
            
            for compny in companies {
                let latitude = compny.coord_x
                let longitude = compny.coord_y
                let id = compny.id
                let point = YMKPoint(latitude: latitude, longitude: longitude)
                let mapobj = mapObjects.addPlacemark(with: point, image: UIImage(named: "foodpin")!)
                mapobj.userData = id
                mapObjectsArr.append(mapobj)
            }
        } else {
            let company = ApiWorker.sharedInstance.companies.first(where: {$0.id == ApiWorker.sharedInstance.selectedCompanyId})!
            let latitude = Double(company.coord_x)
            let longitude = Double(company.coord_y)
            let id = company.id
            let point = YMKPoint(latitude: latitude, longitude: longitude)
            let mapobj = mapObjects.addPlacemark(with: point, image: UIImage(named: "foodpin")!)
            mapobj.userData = id
            mapObjectsArr.append(mapobj)
        }
    }
    
    func toPhoneViewController() {
        performSegue(withIdentifier: "PhoneViewController", sender: nil)
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print(url)
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.companyImage.image = UIImage(data: data)
            }
        }
    }
    
    func toMenuTableViewController() {
        let tabBar = self.tabBarController
        tabBar!.selectedIndex = 1
    }
    
    func addRout(location: CLLocation) {
        if ApiWorker.sharedInstance.isTrack == true {
            let company = ApiWorker.sharedInstance.companies.first(where: {$0.id == ApiWorker.sharedInstance.selectedCompanyId})!
            let requestPoints : [YMKRequestPoint] = [
                YMKRequestPoint(point: YMKPoint(latitude: company.coord_x, longitude: company.coord_y), type: .waypoint, pointContext: nil),
                YMKRequestPoint(point: YMKPoint(latitude:  location.coordinate.latitude, longitude: location.coordinate.longitude), type: .waypoint, pointContext: nil)
                ]
            
            let responseHandler = {(routesResponse: [YMKDrivingRoute]?, error: Error?) -> Void in
                if let routes = routesResponse {
                    self.onRoutesReceived(routes)
                } else {
                    
                }
            }
            
            let drivingRouter = YMKDirections.sharedInstance().createDrivingRouter()
            drivingSession = drivingRouter.requestRoutes(
                with: requestPoints,
                drivingOptions: YMKDrivingDrivingOptions(initialAzimuth: nil, alternativeCount: 1, offlineHandicap: nil, avoidTolls: nil),
                routeHandler: responseHandler)
            
        }
    }
    
    func onRoutesReceived(_ routes: [YMKDrivingRoute]) {
        let mapObjects = mapView.mapWindow.map.mapObjects
        for route in routes {
            mapObjects.addPolyline(with: route.geometry)
        }
    }
    
    func setCartBadgeCount(count: Int) {
        let item = self.tabBarController!.tabBar.items![2]
        item.badgeColor = UIColor(named: "MainGreen")
        item.badgeValue = String(count)
    }
    
    @objc func getOrderStatus() {
        ApiWorker.sharedInstance.orderStatus(onSuccess: { (error, status) in
            if status == 4 {
                self.time!.invalidate()
                ApiWorker.sharedInstance.isTrack = false
                self.closeCompanyView(completion: {})
                self.viewWillAppear(false)
            }
            self.statusLabel.text = "Статус: " + self.statusDic[status]!
        }) { (error) in
            
        }
    }
    
    func distance(firstPoint: CLLocation, secondPoint: CLLocation) -> Double {
        return Double(firstPoint.distance(from: secondPoint) / 1000)
    }
    
    
}
