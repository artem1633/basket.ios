//
//  Product.swift
//  vPooty
//
//  Created by Alexey Voronov on 03/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation

struct Product{
    let id: Int
    let name: String
    let description: String
    let picture: String
    let weight: String
    let category_product_id: Int
    let price: String
}
