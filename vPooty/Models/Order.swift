//
//  Order.swift
//  vPooty
//
//  Created by Alexey Voronov on 07/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation

struct Order {
    let companyId: Int?
    let status: Int?
    let order_id: Int
    let time_max: Int
    let summa: Float
    let order_date: String?
}
