//
//  CarInfo.swift
//  vPooty
//
//  Created by Alexey Voronov on 06/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation

struct CarInfo {
    let car_type: String
    let car_number: String
    let car_color: String
    let car_model: String
}
