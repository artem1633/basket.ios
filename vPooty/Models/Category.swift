//
//  Category.swift
//  vPooty
//
//  Created by Alexey Voronov on 01/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation

struct Category {
    let id: Int
    let name: String
}
