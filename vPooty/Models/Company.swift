//
//  Company.swift
//  vPooty
//
//  Created by Alexey Voronov on 01/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation

struct Company {
    let id: Int
    let name: String
    let description: String
    let picture: String
    let coord_x: Double
    let coord_y: Double
}
