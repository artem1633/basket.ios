//
//  ProductInCart.swift
//  vPooty
//
//  Created by Alexey Voronov on 05/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation


struct ProductInCart {
    var count: Int
    var product: Product
}
