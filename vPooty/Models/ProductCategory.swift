//
//  ProductCategory.swift
//  vPooty
//
//  Created by Alexey Voronov on 02/02/2019.
//  Copyright © 2019 Alexey Voronov. All rights reserved.
//

import Foundation

struct ProductCategory {
    let id: Int
    let name: String
}
