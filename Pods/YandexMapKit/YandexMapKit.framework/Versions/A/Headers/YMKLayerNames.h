#import <YandexRuntime/YRTPlatformBinding.h>

@interface YMKLayerNames : YRTPlatformBinding

+ (NSString *)mapLayerName;

+ (NSString *)jamsLayerName;

+ (NSString *)personalizedPoiLayerName;

+ (NSString *)carparksLayerName;

+ (NSString *)indoorLayerName;

+ (NSString *)transportLayerName;

+ (NSString *)searchPinsLayerName;

+ (NSString *)advertPinsLayerName;

+ (NSString *)buildingsLayerName;

+ (NSString *)mapObjectsLayerName;

+ (NSString *)userLocationLayerName;

@end
