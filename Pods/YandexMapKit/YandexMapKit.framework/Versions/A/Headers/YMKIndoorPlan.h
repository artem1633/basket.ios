#import <YandexMapKit/YMKIndoorLevel.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
@interface YMKIndoorPlan : YRTPlatformBinding

/**
 * List of all levels in indoor plan, from bottom to top.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKIndoorLevel *> *levels;

/**
 * Gets/sets active level.
 */
@property (nonatomic) NSInteger activeLevelIndex;

@end
/// @endcond

