#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, YMKLocationFilteringMode) {

    /**
     * Locations should be filtered (no spoofed locations and locations from
     * the past)
     */
    YMKLocationFilteringModeOn,

    /**
     * Only invalid (zero) locations should be filtered
     */
    YMKLocationFilteringModeOff
};

