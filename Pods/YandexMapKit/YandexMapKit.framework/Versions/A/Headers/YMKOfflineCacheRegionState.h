#import <Foundation/Foundation.h>

/**
 * @attention This feature is not available in the free MapKit version.
 *
 *
 * The state of the region.
 */
typedef NS_ENUM(NSUInteger, YMKOfflineCacheRegionState) {

    /**
     * Available for download on server.
     */
    YMKOfflineCacheRegionStateAvailable,

    /**
     * Download in progress.
     */
    YMKOfflineCacheRegionStateDownloading,

    /**
     * Download is paused.
     */
    YMKOfflineCacheRegionStatePaused,

    /**
     * Cache data installation is finished.
     */
    YMKOfflineCacheRegionStateCompleted,

    /**
     * The region was completed but there is a newer version on server
     */
    YMKOfflineCacheRegionStateOutdated
};

