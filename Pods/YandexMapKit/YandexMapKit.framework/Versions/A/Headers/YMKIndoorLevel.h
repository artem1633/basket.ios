#import <Foundation/Foundation.h>

/// @cond EXCLUDE
@interface YMKIndoorLevel : NSObject

/**
 * Localized display name of the level.
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * true if level is under ground
 */
@property (nonatomic, readonly) BOOL isUnderground;


+ (nonnull YMKIndoorLevel *)indoorLevelWithName:(nonnull NSString *)name
                                  isUnderground:( BOOL)isUnderground;


@end
/// @endcond

