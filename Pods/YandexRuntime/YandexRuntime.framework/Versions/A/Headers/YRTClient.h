#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
@interface YRTClient : YRTPlatformBinding

- (void)handleWithData:(nonnull NSData *)data;


@end
/// @endcond

