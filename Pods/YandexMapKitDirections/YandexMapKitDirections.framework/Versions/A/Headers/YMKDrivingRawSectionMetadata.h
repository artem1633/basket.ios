#import <YandexMapKitDirections/YMKDrivingAnnotation.h>
#import <YandexMapKitDirections/YMKDrivingRawAnnotationSchemes.h>
#import <YandexMapKitDirections/YMKDrivingRawJams.h>
#import <YandexMapKitDirections/YMKDrivingRawLaneSigns.h>
#import <YandexMapKitDirections/YMKDrivingRawRestrictedTurns.h>
#import <YandexMapKitDirections/YMKDrivingRawRuggedRoads.h>
#import <YandexMapKitDirections/YMKDrivingRawSpeedLimits.h>
#import <YandexMapKitDirections/YMKDrivingRawSpots.h>
#import <YandexMapKitDirections/YMKDrivingRawTollRoads.h>
#import <YandexMapKitDirections/YMKDrivingWeight.h>

@interface YMKDrivingRawSectionMetadata : NSObject

@property (nonatomic, readonly) NSUInteger legIndex;

@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

@property (nonatomic, readonly, nonnull) YMKDrivingAnnotation *annotation;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *viaPointPositions;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawSpeedLimits *speedLimits;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawAnnotationSchemes *annotationSchemes;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawLaneSigns *laneSigns;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawSpots *spots;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawJams *jams;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawTollRoads *tollRoads;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawRuggedRoads *ruggedRoads;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRawRestrictedTurns *restrictedTurns;


+ (nonnull YMKDrivingRawSectionMetadata *)rawSectionMetadataWithLegIndex:( NSUInteger)legIndex
                                                                  weight:(nonnull YMKDrivingWeight *)weight
                                                              annotation:(nonnull YMKDrivingAnnotation *)annotation
                                                       viaPointPositions:(nonnull NSArray<NSNumber *> *)viaPointPositions
                                                             speedLimits:(nullable YMKDrivingRawSpeedLimits *)speedLimits
                                                       annotationSchemes:(nullable YMKDrivingRawAnnotationSchemes *)annotationSchemes
                                                               laneSigns:(nullable YMKDrivingRawLaneSigns *)laneSigns
                                                                   spots:(nullable YMKDrivingRawSpots *)spots
                                                                    jams:(nullable YMKDrivingRawJams *)jams
                                                               tollRoads:(nullable YMKDrivingRawTollRoads *)tollRoads
                                                             ruggedRoads:(nullable YMKDrivingRawRuggedRoads *)ruggedRoads
                                                         restrictedTurns:(nullable YMKDrivingRawRestrictedTurns *)restrictedTurns;


@end

