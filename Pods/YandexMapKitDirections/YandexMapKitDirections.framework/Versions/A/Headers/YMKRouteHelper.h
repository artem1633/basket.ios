#import <YandexMapKitDirections/YMKJamStyle.h>
#import <YandexMapKitDirections/YMKManeuverStyle.h>

#import <YandexRuntime/YRTPlatformBinding.h>

#import <YandexMapKit/YMKColoredPolylineMapObject.h>

@class YMKDrivingRoute;

@interface YMKRouteHelper : YRTPlatformBinding

/**
 * Creates a default traffic style. Default values: colors : Blocked  :
 * 0x000000ff, Free     : 0x00ff00ff, Hard     : 0xff0000ff, Light    :
 * 0xffff00ff, Unknown  : 0x909090ff, VeryHard : 0xa00000ff
 */
+ (nonnull YMKJamStyle *)createDefaultJamStyle;


/**
 * Creates a disabled traffic jams style. Colors : Blocked  :
 * 0x909090ff, Free     : 0x909090ff, Hard     : 0x909090ff, Light    :
 * 0x909090ff, Unknown  : 0x909090ff, VeryHard : 0x909090ff
 */
+ (nonnull YMKJamStyle *)createDisabledJamStyle;


/**
 * Creates a default maneuver style. Default values: fillColor      :
 * 0x000000ff outlineColor   : 0xffffffff outlineWidth   : 2.f length
 * : 80 triangleHeight : 16 enabled        : false
 */
+ (nonnull YMKManeuverStyle *)createDefaultManeuverStyle;


/**
 * Sets a new geometry and colors for a polyline. If style.enabled =
 * false, the polyline is drawn using the color for the Unknown traffic
 * jam type.
 */
+ (void)updatePolylineWithPolyline:(nonnull YMKColoredPolylineMapObject *)polyline
                             route:(nonnull YMKDrivingRoute *)route
                             style:(nonnull YMKJamStyle *)style;


/**
 * Applies the traffic jam style. Updates colors for traffic jam types
 * provided in the style parameter. Default color is 0xffffffff.
 */
+ (void)applyJamStyleWithPolyline:(nonnull YMKColoredPolylineMapObject *)polyline
                            style:(nonnull YMKJamStyle *)style;


/**
 * Applies a maneuver style. This method should be called every time
 * after updatePolyline occurs; otherwise, the default maneuver style is
 * applied.
 */
+ (void)applyManeuverStyleWithPolyline:(nonnull YMKColoredPolylineMapObject *)polyline
                                 style:(nonnull YMKManeuverStyle *)style;


@end
