#import <YandexMapKitDirections/YMKDrivingRawRestrictedTurn.h>

@interface YMKDrivingRawRestrictedTurns : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRawRestrictedTurn *> *restrictedTurns;


+ (nonnull YMKDrivingRawRestrictedTurns *)rawRestrictedTurnsWithRestrictedTurns:(nonnull NSArray<YMKDrivingRawRestrictedTurn *> *)restrictedTurns;


@end

