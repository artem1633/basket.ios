#import <YandexMapKitDirections/YMKDrivingFlags.h>
#import <YandexMapKitDirections/YMKDrivingWeight.h>

/**
 * Route summary.
 */
@interface YMKDrivingSummary : NSObject

/**
 * Route "weight".
 */
@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

/**
 * Overall route characteristics.
 */
@property (nonatomic, readonly, nonnull) YMKDrivingFlags *flags;


+ (nonnull YMKDrivingSummary *)summaryWithWeight:(nonnull YMKDrivingWeight *)weight
                                           flags:(nonnull YMKDrivingFlags *)flags;


@end

