#import <YandexMapKitDirections/YMKDrivingAnnotationLang.h>
#import <YandexMapKitDirections/YMKDrivingDrivingOptions.h>
#import <YandexMapKitDirections/YMKDrivingSession.h>
#import <YandexMapKitDirections/YMKDrivingSummarySession.h>
#import <YandexMapKitDirections/YMKDrivingVehicleType.h>
#import <YandexMapKitDirections/YMKDrivingVehicleTypeListener.h>

#import <YandexRuntime/YRTPlatformBinding.h>

#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKPolylinePosition.h>
#import <YandexMapKit/YMKRequestPoint.h>

@class YMKDrivingRoute;
@class YMKDrivingRouteSerializer;

/**
 * Interface for the driving router.
 */
@interface YMKDrivingRouter : YRTPlatformBinding

/**
 * Builds a route.
 *
 * @param points Route points.
 * @param drivingOptions Driving options.
 * @param routeListener Route listener object.
 */
- (nonnull YMKDrivingSession *)requestRoutesWithPoints:(nonnull NSArray<YMKRequestPoint *> *)points
                                        drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                          routeHandler:(nonnull YMKDrivingSessionRouteHandler)routeHandler;


/**
 * Creates a route summary.
 *
 * @param points Route points.
 * @param drivingOptions Driving options.
 * @param summaryListener Route listener object.
 */
- (nonnull YMKDrivingSummarySession *)requestRoutesSummaryWithPoints:(nonnull NSArray<YMKRequestPoint *> *)points
                                                      drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                                      summaryHandler:(nonnull YMKDrivingSummarySessionSummaryHandler)summaryHandler;


/// @cond EXCLUDE
/**
 * Creates alternatives for a built route.
 *
 * @param route Initial route.
 * @param routePosition Route position.
 * @param drivingOptions Driving options.
 * @param routeListener Route listener object.
 */
- (nonnull YMKDrivingSession *)requestAlternativesForRouteWithRoute:(nonnull YMKDrivingRoute *)route
                                                      routePosition:(nonnull YMKPolylinePosition *)routePosition
                                                     drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                                       routeHandler:(nonnull YMKDrivingSessionRouteHandler)routeHandler;
/// @endcond


/// @cond EXCLUDE
/**
 *
 *
 * Remark:
 * @param finish has optional type, it may be uninitialized.
 */
- (nonnull YMKDrivingSession *)requestParkingRoutesWithLocation:(nonnull YMKPoint *)location
                                                         finish:(nullable YMKPoint *)finish
                                                 drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                                   routeHandler:(nonnull YMKDrivingSessionRouteHandler)routeHandler;
/// @endcond


/**
 * Route serializer.
 */
- (nonnull YMKDrivingRouteSerializer *)routeSerializer;


/**
 * A method to set the annotation language.
 *
 * @param annotationLanguage The annotation language.
 */
- (void)setAnnotationLanguageWithAnnotationLanguage:(YMKDrivingAnnotationLanguage)annotationLanguage;


/**
 * @attention This feature is not available in the free MapKit version.
 *
 *
 * Enables or disables offline routing. Disabled by default.
 */
- (void)setOfflineRoutingEnabledWithOn:(BOOL)on;


/// @cond EXCLUDE
/**
 * Sets the vehicle type for special routing.
 */
- (void)setVehicleTypeWithVehicleType:(YMKDrivingVehicleType)vehicleType;
/// @endcond


/// @cond EXCLUDE
- (YMKDrivingVehicleType)vehicleType;
/// @endcond


/// @cond EXCLUDE
- (void)addVehicleTypeListenerWithVehicleTypeListener:(nonnull id<YMKDrivingVehicleTypeListener>)vehicleTypeListener;
/// @endcond


/// @cond EXCLUDE
- (void)removeVehicleTypeListenerWithVehicleTypeListener:(nonnull id<YMKDrivingVehicleTypeListener>)vehicleTypeListener;
/// @endcond


/**
 * Suspends all conditions in the request for routes received by this
 * DrivingRouter.
 */
- (void)suspend;


/**
 * Resumes all conditions in the request for routes received by this
 * DrivingRouter. This is the default state.
 */
- (void)resume;


@end

