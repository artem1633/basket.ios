#import <YandexMapKitDirections/YMKDrivingAnnotation.h>
#import <YandexMapKitDirections/YMKDrivingWeight.h>

/**
 * Information about section metadata.
 */
@interface YMKDrivingSectionMetadata : NSObject

/**
 * A leg is a section of the route between two consecutive waypoints.
 */
@property (nonatomic, readonly) NSUInteger legIndex;

/**
 * The "weight" of the section.
 */
@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

/**
 * Section annotations.
 */
@property (nonatomic, readonly, nonnull) YMKDrivingAnnotation *annotation;

/**
 * The ID of the annotation scheme.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *schemeId;

/**
 * Throughpoints can appear only at nodes of the section's geometry. The
 * vector contains the positions of all corresponding nodes. These
 * positions should be listed in ascending order.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *viaPointPositions;


+ (nonnull YMKDrivingSectionMetadata *)sectionMetadataWithLegIndex:( NSUInteger)legIndex
                                                            weight:(nonnull YMKDrivingWeight *)weight
                                                        annotation:(nonnull YMKDrivingAnnotation *)annotation
                                                          schemeId:(nullable NSNumber *)schemeId
                                                 viaPointPositions:(nonnull NSArray<NSNumber *> *)viaPointPositions;


@end

