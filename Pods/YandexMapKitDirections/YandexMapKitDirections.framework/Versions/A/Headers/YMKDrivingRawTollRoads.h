#import <YandexMapKitDirections/YMKDrivingTollRoad.h>

@interface YMKDrivingRawTollRoads : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingTollRoad *> *tollRoads;


+ (nonnull YMKDrivingRawTollRoads *)rawTollRoadsWithTollRoads:(nonnull NSArray<YMKDrivingTollRoad *> *)tollRoads;


@end

