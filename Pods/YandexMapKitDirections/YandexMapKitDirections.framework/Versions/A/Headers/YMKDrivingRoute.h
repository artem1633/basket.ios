#import <YandexMapKitDirections/YMKDrivingAnnotation.h>
#import <YandexMapKitDirections/YMKDrivingAnnotationLang.h>
#import <YandexMapKitDirections/YMKDrivingConditionsListener.h>
#import <YandexMapKitDirections/YMKDrivingEvent.h>
#import <YandexMapKitDirections/YMKDrivingJamSegment.h>
#import <YandexMapKitDirections/YMKDrivingLaneSign.h>
#import <YandexMapKitDirections/YMKDrivingRestrictedTurn.h>
#import <YandexMapKitDirections/YMKDrivingRouteMetadata.h>
#import <YandexMapKitDirections/YMKDrivingRuggedRoad.h>
#import <YandexMapKitDirections/YMKDrivingSection.h>
#import <YandexMapKitDirections/YMKDrivingSpot.h>
#import <YandexMapKitDirections/YMKDrivingTollRoad.h>
#import <YandexMapKitDirections/YMKDrivingVehicleType.h>

#import <YandexRuntime/YRTPlatformBinding.h>

#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKPolylinePosition.h>
#import <YandexMapKit/YMKRequestPoint.h>

/**
 * Driving route. A route consists of multiple sections. Each section
 * has a corresponding annotation that describes the action at the
 * beginning of the section.
 */
@interface YMKDrivingRoute : YRTPlatformBinding

/**
 * The ID of the route.
 */
@property (nonatomic, readonly, nonnull) NSString *routeId;

/**
 * The route metadata.
 */
@property (nonatomic, readonly, nonnull) YMKDrivingRouteMetadata *metadata;

/**
 * WayPoint-to-WayPoint route sections.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingSection *> *sections;

/**
 * Route geometry.
 */
@property (nonatomic, readonly, nonnull) YMKPolyline *geometry;

/**
 * Traffic conditions on the given route.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingJamSegment *> *jamSegments;

/**
 * Events on the given route.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingEvent *> *events;

/**
 * Speed limits for segments in the geometry.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *speedLimits;

/**
 * Annotation schemes for segments in the geometry.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *annotationSchemes;

/**
 * Vehicle type (e.g. Taxi).
 */
@property (nonatomic, readonly) YMKDrivingVehicleType vehicleType;

/**
 * Lane signs.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingLaneSign *> *laneSigns;

/**
 * Route spots.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingSpot *> *spots;

/**
 * Route points with time-dependent restrictions.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRestrictedTurn *> *restrictedTurns;

/**
 * Route roads.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRuggedRoad *> *ruggedRoads;

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingTollRoad *> *tollRoads;

/**
 * Language of string annotations (e.g. street names) in this route
 * object.
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) NSNumber *annotationLanguage;

/**
 * Request points that were specified in the router request that this
 * route originated from.
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) NSArray<YMKRequestPoint *> *requestPoints;

/**
 * The reached position on the given route. The 'RouteMetadata::weight'
 * field contains data for the part of the route beyond this position.
 */
@property (nonatomic, nonnull) YMKPolylinePosition *position;

/**
 * Indicates whether driving conditions (jamSegments and events) have
 * become outdated when we are not able to fetch updates for some
 * predefined time.
 */
@property (nonatomic, readonly, getter=isAreConditionsOutdated) BOOL areConditionsOutdated;

/**
 * Adds a listener for route condition changes.
 */
- (void)addConditionsListenerWithConditionsListener:(nonnull id<YMKDrivingConditionsListener>)conditionsListener;


/**
 * Removes a listener for route condition changes.
 */
- (void)removeConditionsListenerWithConditionsListener:(nonnull id<YMKDrivingConditionsListener>)conditionsListener;


/**
 * Section index.
 */
- (NSUInteger)sectionIndexWithSegmentIndex:(NSUInteger)segmentIndex;


/**
 * Metadata location.
 */
- (nonnull YMKDrivingRouteMetadata *)metadataAtWithPosition:(nonnull YMKPolylinePosition *)position;


@end

