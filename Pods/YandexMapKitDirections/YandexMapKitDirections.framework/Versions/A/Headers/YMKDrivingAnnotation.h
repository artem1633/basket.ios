#import <YandexMapKitDirections/YMKDrivingAction.h>
#import <YandexMapKitDirections/YMKDrivingActionMetadata.h>
#import <YandexMapKitDirections/YMKDrivingLandmark.h>
#import <YandexMapKitDirections/YMKDrivingToponymPhrase.h>

/**
 * The identifier of the annotation scheme.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingAnnotationSchemeID) {

    /**
     * Small annotation.
     */
    YMKDrivingAnnotationSchemeIDSmall,

    /**
     * Medium annotation.
     */
    YMKDrivingAnnotationSchemeIDMedium,

    /**
     * Large annotation.
     */
    YMKDrivingAnnotationSchemeIDLarge,

    /**
     * Highway annotation.
     */
    YMKDrivingAnnotationSchemeIDHighway
};


/**
 * The annotation that is displayed on the map.
 */
@interface YMKDrivingAnnotation : NSObject

/**
 * Driver action.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *action;

/**
 * The toponym of the location.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *toponym;

/**
 * Description text to display.
 */
@property (nonatomic, readonly, nonnull) NSString *descriptionText;

/**
 * Action metadata.
 */
@property (nonatomic, readonly, nonnull) YMKDrivingActionMetadata *actionMetadata;

/**
 * Significant landmarks.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *landmarks;

/**
 * The description of the object.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingToponymPhrase *toponymPhrase;


+ (nonnull YMKDrivingAnnotation *)annotationWithAction:(nullable NSNumber *)action
                                               toponym:(nullable NSString *)toponym
                                       descriptionText:(nonnull NSString *)descriptionText
                                        actionMetadata:(nonnull YMKDrivingActionMetadata *)actionMetadata
                                             landmarks:(nonnull NSArray<NSNumber *> *)landmarks
                                         toponymPhrase:(nullable YMKDrivingToponymPhrase *)toponymPhrase;


@end

