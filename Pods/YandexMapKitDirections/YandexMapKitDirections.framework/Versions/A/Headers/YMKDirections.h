#import <YandexMapKitDirections/YMKCarparksLayer.h>
#import <YandexMapKitDirections/YMKCarparksNearbyLayer.h>
#import <YandexMapKitDirections/YMKDrivingRouter.h>
#import <YandexMapKitDirections/YMKGuidanceGuide.h>
#import <YandexMapKitDirections/YMKRecordedSimulator.h>

#import <YandexRuntime/YRTPlatformBinding.h>
#import <YandexRuntime/YRTReportData.h>
#import <YandexRuntime/YRTReportFactory.h>

#import <YandexMapKit/YMKMapWindow.h>

@interface YMKDirections : YRTPlatformBinding

/// @cond EXCLUDE
/**
 * Creates the carparks layer.
 */
- (nonnull YMKCarparksLayer *)createCarparksLayerWithMapWindow:(nonnull YMKMapWindow *)mapWindow;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates the nearby carparks layer.
 */
- (nonnull YMKCarparksNearbyLayer *)createCarparksNearbyLayerWithMapWindow:(nonnull YMKMapWindow *)mapWindow;
/// @endcond


/**
 * Creates a manager that builds driving routes using the specified
 * endpoints and other route parameters.
 */
- (nonnull YMKDrivingRouter *)createDrivingRouter;


/// @cond EXCLUDE
/**
 * Creates a guide.
 */
- (nonnull YMKGuidanceGuide *)createGuide;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a suspended recorded simulator object with the given report.
 */
- (nonnull YMKRecordedSimulator *)createRecordedSimulatorWithReportData:(nonnull YRTReportData *)reportData;
/// @endcond


/// @cond EXCLUDE
/**
 * Creates a report factory.
 */
- (nonnull YRTReportFactory *)createReportFactory;
/// @endcond


/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

