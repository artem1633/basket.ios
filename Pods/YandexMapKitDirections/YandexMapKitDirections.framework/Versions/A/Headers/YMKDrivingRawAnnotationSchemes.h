#import <YandexMapKitDirections/YMKDrivingRawAnnotationScheme.h>

@interface YMKDrivingRawAnnotationSchemes : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRawAnnotationScheme *> *schemes;


+ (nonnull YMKDrivingRawAnnotationSchemes *)rawAnnotationSchemesWithSchemes:(nonnull NSArray<YMKDrivingRawAnnotationScheme *> *)schemes;


@end

