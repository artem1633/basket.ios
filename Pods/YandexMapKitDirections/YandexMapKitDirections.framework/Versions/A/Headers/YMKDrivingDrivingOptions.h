#import <YandexMapKitDirections/YMKDrivingDrivingRouter.h>

/**
 * Driving options.
 */
@interface YMKDrivingDrivingOptions : NSObject

/**
 * Starting location azimuth.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *initialAzimuth;

/**
 * The number of alternatives.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *alternativeCount;

/**
 * The maximum time a user waits for the online route if the offline
 * route is ready.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *offlineHandicap;

/**
 * The 'avoidTolls' option instructs the router to return routes that
 * avoid tolls, when possible.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *avoidTolls;

+ (nonnull YMKDrivingDrivingOptions *)drivingOptionsWithInitialAzimuth:(nullable NSNumber *)initialAzimuth
                                                      alternativeCount:(nullable NSNumber *)alternativeCount
                                                       offlineHandicap:(nullable NSNumber *)offlineHandicap
                                                            avoidTolls:(nullable NSNumber *)avoidTolls;


@end
