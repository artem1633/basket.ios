#import <YandexMapKitDirections/YMKDrivingRoute.h>
#import <YandexMapKitDirections/YMKRecordedSimulatorListener.h>

#import <YandexMapKit/YMKLocation.h>
#import <YandexMapKit/YMKLocationManager.h>

/// @cond EXCLUDE
@interface YMKRecordedSimulator : YMKLocationManager

@property (nonatomic, nonnull) NSDate *timestamp;

@property (nonatomic) NSInteger clockRate;

/**
 * true if simulator is not suspended
 */
@property (nonatomic, readonly, getter=isActive) BOOL active;

/**
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKLocation *location;

/**
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKDrivingRoute *route;

- (void)subscribeForSimulatorEventsWithSimulatorListener:(nonnull id<YMKRecordedSimulatorListener>)simulatorListener;


- (void)unsubscribeFromSimulatorEventsWithSimulatorListener:(nonnull id<YMKRecordedSimulatorListener>)simulatorListener;


@end
/// @endcond

