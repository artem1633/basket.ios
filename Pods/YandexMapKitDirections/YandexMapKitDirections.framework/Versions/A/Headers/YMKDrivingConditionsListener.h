#import <Foundation/Foundation.h>

/**
 * A listener to monitor route condition changes.
 */
@protocol YMKDrivingConditionsListener <NSObject>

/**
 * Triggers when the conditions are updated.
 */
- (void)onConditionsUpdated;


/**
 * Triggers when the conditions are outdated.
 */
- (void)onConditionsOutdated;


@end
