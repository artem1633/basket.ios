#import <YandexMapKitDirections/YMKDrivingDescription.h>
#import <YandexMapKitDirections/YMKDrivingFlags.h>
#import <YandexMapKitDirections/YMKDrivingRoutePoint.h>
#import <YandexMapKitDirections/YMKDrivingWeight.h>

/**
 * Information about driving route metadata.
 */
@interface YMKDrivingRouteMetadata : NSObject

/**
 * Route "weight".
 */
@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

/**
 * Overall route characteristics.
 */
@property (nonatomic, readonly, nonnull) YMKDrivingFlags *flags;

/**
 * Route descriptor.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSData *descriptor;

/**
 * Route traits.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSData *traits;

/**
 * Route description.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKDrivingDescription *description;

/**
 * Selected route arrival points.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRoutePoint *> *routePoints;

/**
 * Route tags.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *tags;


+ (nonnull YMKDrivingRouteMetadata *)routeMetadataWithWeight:(nonnull YMKDrivingWeight *)weight
                                                       flags:(nonnull YMKDrivingFlags *)flags
                                                  descriptor:(nullable NSData *)descriptor
                                                      traits:(nullable NSData *)traits
                                                 description:(nullable YMKDrivingDescription *)description
                                                 routePoints:(nonnull NSArray<YMKDrivingRoutePoint *> *)routePoints
                                                        tags:(nonnull NSArray<NSString *> *)tags;


@end

