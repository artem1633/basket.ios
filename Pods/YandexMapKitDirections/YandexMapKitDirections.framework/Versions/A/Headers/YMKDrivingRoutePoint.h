#import <YandexMapKit/YMKPoint.h>

/**
 * Route point metadata (exists for both waypoints and via points).
 */
@interface YMKDrivingRoutePoint : NSObject

/**
 * Position of original route point.
 */
@property (nonatomic, readonly, nonnull) YMKPoint *position;

/**
 * Arrival point selected for routing.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKPoint *selectedArrivalPoint;

/**
 * ID of driving arrival point selected for routing.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *drivingArrivalPointId;


+ (nonnull YMKDrivingRoutePoint *)routePointWithPosition:(nonnull YMKPoint *)position
                                    selectedArrivalPoint:(nullable YMKPoint *)selectedArrivalPoint
                                   drivingArrivalPointId:(nullable NSString *)drivingArrivalPointId;


@end

