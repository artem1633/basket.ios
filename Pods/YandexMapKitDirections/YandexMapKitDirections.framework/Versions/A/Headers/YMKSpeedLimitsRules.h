#import <Foundation/Foundation.h>

/// @cond EXCLUDE
typedef NS_ENUM(NSUInteger, YMKSpeedLimitsRulesType) {

    YMKSpeedLimitsRulesTypeAbsolute,

    YMKSpeedLimitsRulesTypeRelative
};


@interface YMKSpeedLimitsRules : NSObject

@property (nonatomic, readonly) YMKSpeedLimitsRulesType urban;

@property (nonatomic, readonly) YMKSpeedLimitsRulesType rural;

@property (nonatomic, readonly) YMKSpeedLimitsRulesType expressway;


+ (nonnull YMKSpeedLimitsRules *)speedLimitsRulesWithUrban:( YMKSpeedLimitsRulesType)urban
                                                     rural:( YMKSpeedLimitsRulesType)rural
                                                expressway:( YMKSpeedLimitsRulesType)expressway;


@end
/// @endcond

