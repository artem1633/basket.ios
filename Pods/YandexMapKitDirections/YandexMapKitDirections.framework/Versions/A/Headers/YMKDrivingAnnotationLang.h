#import <Foundation/Foundation.h>

/**
 * The language of the annotations.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingAnnotationLanguage) {

    /**
     * Russian language.
     */
    YMKDrivingAnnotationLanguageRussian,

    /**
     * English language.
     */
    YMKDrivingAnnotationLanguageEnglish,

    /**
     * French language.
     */
    YMKDrivingAnnotationLanguageFrench,

    /**
     * Turkish language.
     */
    YMKDrivingAnnotationLanguageTurkish,

    /**
     * Ukrainian language.
     */
    YMKDrivingAnnotationLanguageUkrainian,

    /**
     * Italian language.
     */
    YMKDrivingAnnotationLanguageItalian,

    /**
     * The number of annotation languages.
     */
    YMKDrivingAnnotationLanguageAnnotationLanguage_Count
};

