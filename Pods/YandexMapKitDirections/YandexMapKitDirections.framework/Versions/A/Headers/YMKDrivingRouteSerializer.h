#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKDrivingRoute;

/**
 * Route serializer interface.
 */
@interface YMKDrivingRouteSerializer : YRTPlatformBinding

/**
 * This method saves the route.
 */
- (nonnull NSData *)saveWithRoute:(nonnull YMKDrivingRoute *)route;


/**
 * This method returns null if given a saved route from an incompatible
 * version of MapKit.
 */
- (nonnull YMKDrivingRoute *)loadWithData:(nonnull NSData *)data;


@end

