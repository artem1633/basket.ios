#import <YandexMapKitDirections/YMKDrivingAnnotation.h>

@interface YMKDrivingRawAnnotationScheme : NSObject

@property (nonatomic, readonly) NSInteger count;

@property (nonatomic, readonly) YMKDrivingAnnotationSchemeID schemeId;


+ (nonnull YMKDrivingRawAnnotationScheme *)rawAnnotationSchemeWithCount:( NSInteger)count
                                                               schemeId:( YMKDrivingAnnotationSchemeID)schemeId;


@end

