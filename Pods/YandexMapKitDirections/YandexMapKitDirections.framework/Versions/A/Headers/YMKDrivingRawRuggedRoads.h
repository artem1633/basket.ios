#import <YandexMapKitDirections/YMKDrivingRuggedRoad.h>

@interface YMKDrivingRawRuggedRoads : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRuggedRoad *> *ruggedRoads;


+ (nonnull YMKDrivingRawRuggedRoads *)rawRuggedRoadsWithRuggedRoads:(nonnull NSArray<YMKDrivingRuggedRoad *> *)ruggedRoads;


@end

