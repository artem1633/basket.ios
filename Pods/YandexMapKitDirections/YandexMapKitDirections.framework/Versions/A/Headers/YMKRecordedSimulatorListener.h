#import <Foundation/Foundation.h>

/// @cond EXCLUDE
@protocol YMKRecordedSimulatorListener <NSObject>

- (void)onLocationUpdated;


- (void)onRouteUpdated;


- (void)onProblemMark;


- (void)onFinish;


@end
/// @endcond
