#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKDrivingRoute;

typedef void(^YMKDrivingSessionRouteHandler)(
    NSArray<YMKDrivingRoute *> *routes,
    NSError *error);

/**
 * Driving session information.
 */
@interface YMKDrivingSession : YRTPlatformBinding

/**
 * Cancels the driving session.
 */
- (void)cancel;


/**
 * Tries to create a driving session again.
 */
- (void)retryWithRouteHandler:(nonnull YMKDrivingSessionRouteHandler)routeHandler;


@end

