#import <YandexMapKitDirections/YMKDrivingAnnotation.h>

#import <YandexMapKit/YMKLocalizedValue.h>
#import <YandexMapKit/YMKPolylinePosition.h>

/// @cond EXCLUDE
@interface YMKGuidanceAnnotationWithDistance : NSObject

@property (nonatomic, readonly, nonnull) YMKDrivingAnnotation *annotation;

@property (nonatomic, readonly, nonnull) YMKLocalizedValue *distance;

@property (nonatomic, readonly, nonnull) YMKPolylinePosition *position;


+ (nonnull YMKGuidanceAnnotationWithDistance *)annotationWithDistanceWithAnnotation:(nonnull YMKDrivingAnnotation *)annotation
                                                                           distance:(nonnull YMKLocalizedValue *)distance
                                                                           position:(nonnull YMKPolylinePosition *)position;


@end
/// @endcond

