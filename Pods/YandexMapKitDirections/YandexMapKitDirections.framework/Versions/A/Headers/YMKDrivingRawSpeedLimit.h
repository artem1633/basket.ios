#import <Foundation/Foundation.h>

@interface YMKDrivingRawSpeedLimit : NSObject

@property (nonatomic, readonly) NSInteger count;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *speed;


+ (nonnull YMKDrivingRawSpeedLimit *)rawSpeedLimitWithCount:( NSInteger)count
                                                      speed:(nullable NSNumber *)speed;


@end

