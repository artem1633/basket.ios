#import <YandexMapKitDirections/YMKDrivingJamSegment.h>

#import <UIKit/UIKit.h>

/**
 * Color for specific level of traffic intensity.
 */
@interface YMKJamTypeColor : NSObject

/**
 * The type of the traffic jam.
 */
@property (nonatomic, readonly) YMKDrivingJamType jamType;

/**
 * Traffic jam color.
 */
@property (nonatomic, readonly, nonnull) UIColor *jamColor;


+ (nonnull YMKJamTypeColor *)jamTypeColorWithJamType:( YMKDrivingJamType)jamType
                                            jamColor:(nonnull UIColor *)jamColor;


@end

