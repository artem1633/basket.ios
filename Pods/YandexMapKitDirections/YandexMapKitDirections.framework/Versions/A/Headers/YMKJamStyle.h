#import <YandexMapKitDirections/YMKJamTypeColor.h>

/**
 * The style that is used to display traffic intensity.
 */
@interface YMKJamStyle : NSObject

/**
 * Collection of colors for traffic intensity.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKJamTypeColor *> *colors;


+ (nonnull YMKJamStyle *)jamStyleWithColors:(nonnull NSArray<YMKJamTypeColor *> *)colors;


@end

