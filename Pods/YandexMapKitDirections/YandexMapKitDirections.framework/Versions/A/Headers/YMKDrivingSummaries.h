#import <YandexMapKitDirections/YMKDrivingSummary.h>

/**
 * A list of route segment summaries.
 */
@interface YMKDrivingSummaries : NSObject

/**
 * Route summaries.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingSummary *> *summaries;


+ (nonnull YMKDrivingSummaries *)summariesWithSummaries:(nonnull NSArray<YMKDrivingSummary *> *)summaries;


@end

