#import <Foundation/Foundation.h>

/**
 * The number of the exit for leaving the roundabout.
 */
@interface YMKDrivingLeaveRoundaboutMetadata : NSObject

/**
 * The exit number.
 */
@property (nonatomic, readonly) NSUInteger exitNumber;


+ (nonnull YMKDrivingLeaveRoundaboutMetadata *)leaveRoundaboutMetadataWithExitNumber:( NSUInteger)exitNumber;


@end

