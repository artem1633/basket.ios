#import <YandexMapKit/YMKSubpolyline.h>

@interface YMKDrivingRuggedRoad : NSObject

@property (nonatomic, readonly, nonnull) YMKSubpolyline *position;


+ (nonnull YMKDrivingRuggedRoad *)ruggedRoadWithPosition:(nonnull YMKSubpolyline *)position;


@end

