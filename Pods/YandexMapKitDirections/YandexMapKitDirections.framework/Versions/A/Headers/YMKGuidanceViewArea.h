#import <Foundation/Foundation.h>

/// @cond EXCLUDE
@interface YMKGuidanceViewArea : NSObject

/**
 * In the current direction, in meters.
 */
@property (nonatomic, readonly) double lengthwise;

/**
 * Perpendicular to the current direction from the center, in meters.
 */
@property (nonatomic, readonly) double transverse;


+ (nonnull YMKGuidanceViewArea *)viewAreaWithLengthwise:( double)lengthwise
                                             transverse:( double)transverse;


@end
/// @endcond

