#import <YandexMapKitDirections/YMKDirections.h>

@interface YMKDirections (Factory)

+ (instancetype)directions;
+ (instancetype)sharedInstance;

@end
