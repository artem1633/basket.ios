#import <Foundation/Foundation.h>

/**
 * The maximum time a user waits for the online route if the offline
 * route is ready.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingOfflineHandicap) {

    /**
     * Normal wait time.
     */
    YMKDrivingOfflineHandicapNormal,

    /**
     * Extended wait time.
     */
    YMKDrivingOfflineHandicapLong
};

