#import <YandexRuntime/YRTPlatformBinding.h>

#import <YandexMapKit/YMKLocationViewSource.h>

@class YMKGuidanceGuide;

@interface YMKGuideLocationViewSourceFactory : YRTPlatformBinding

/// @cond EXCLUDE
+ (nonnull YMKLocationViewSource *)createLocationViewSourceWithGuide:(nonnull YMKGuidanceGuide *)guide;
/// @endcond


@end
