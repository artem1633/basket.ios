#import <Foundation/Foundation.h>

@interface YMKDrivingRawRestrictedTurn : NSObject

@property (nonatomic, readonly) NSUInteger position;


+ (nonnull YMKDrivingRawRestrictedTurn *)rawRestrictedTurnWithPosition:( NSUInteger)position;


@end

