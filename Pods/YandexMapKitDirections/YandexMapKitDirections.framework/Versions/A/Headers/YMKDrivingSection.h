#import <YandexMapKitDirections/YMKDrivingSectionMetadata.h>

#import <YandexMapKit/YMKSubpolyline.h>

/**
 * Route section.
 */
@interface YMKDrivingSection : NSObject

/**
 * Metadata information for the route section.
 */
@property (nonatomic, readonly, nonnull) YMKDrivingSectionMetadata *metadata;

/**
 * A polyline of the route section.
 */
@property (nonatomic, readonly, nonnull) YMKSubpolyline *geometry;


+ (nonnull YMKDrivingSection *)sectionWithMetadata:(nonnull YMKDrivingSectionMetadata *)metadata
                                          geometry:(nonnull YMKSubpolyline *)geometry;


@end

