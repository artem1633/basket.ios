#import <YandexMapKitDirections/YMKDrivingAnnotationLang.h>
#import <YandexMapKitDirections/YMKPhraseTokens.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
@interface YMKGuidanceLocalizedPhrase : YRTPlatformBinding

- (NSUInteger)tokensCount;


- (YMKPhraseToken)tokenWithIndex:(NSUInteger)index;


- (nonnull NSString *)text;


- (YMKDrivingAnnotationLanguage)language;


@end
/// @endcond

