#import <Foundation/Foundation.h>

/**
 * A listener to determine vehicle type changes.
 */
@protocol YMKDrivingVehicleTypeListener <NSObject>

/**
 * Called when a vehicle type is updated.
 */
- (void)onVehicleTypeUpdated;


@end
