#import <YandexMapKitDirections/YMKDrivingLane.h>

#import <YandexMapKit/YMKPolylinePosition.h>

/**
 * The sign showing a lane.
 */
@interface YMKDrivingLaneSign : NSObject

/**
 * The position of the sign.
 */
@property (nonatomic, readonly, nonnull) YMKPolylinePosition *position;

/**
 * The lane to display the sign in.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingLane *> *lanes;


+ (nonnull YMKDrivingLaneSign *)laneSignWithPosition:(nonnull YMKPolylinePosition *)position
                                               lanes:(nonnull NSArray<YMKDrivingLane *> *)lanes;


@end

