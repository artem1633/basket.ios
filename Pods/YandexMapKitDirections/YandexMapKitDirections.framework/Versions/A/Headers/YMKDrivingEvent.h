#import <YandexMapKitDirections/YMKDrivingAnnotation.h>

#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKPolylinePosition.h>
#import <YandexMapKit/YMKRoadEventsRoadEvents.h>

/**
 * Road event.
 */
@interface YMKDrivingEvent : NSObject

/**
 * The position of the polyline.
 */
@property (nonatomic, readonly, nonnull) YMKPolylinePosition *polylinePosition;

/**
 * The unique ID of the event.
 */
@property (nonatomic, readonly, nonnull) NSString *eventId;

/**
 * The description of the event.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *descriptionText;

/**
 * The types of the road event.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *types;

/**
 * The location of the road event.
 */
@property (nonatomic, readonly, nonnull) YMKPoint *location;

/**
 * The speed limit on the road.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *speedLimit;

/**
 * valid only for cameras The SchemeId of the annotation.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *annotationSchemeId;


+ (nonnull YMKDrivingEvent *)eventWithPolylinePosition:(nonnull YMKPolylinePosition *)polylinePosition
                                               eventId:(nonnull NSString *)eventId
                                       descriptionText:(nullable NSString *)descriptionText
                                                 types:(nonnull NSArray<NSNumber *> *)types
                                              location:(nonnull YMKPoint *)location
                                            speedLimit:(nullable NSNumber *)speedLimit
                                    annotationSchemeId:(nullable NSNumber *)annotationSchemeId;


@end

