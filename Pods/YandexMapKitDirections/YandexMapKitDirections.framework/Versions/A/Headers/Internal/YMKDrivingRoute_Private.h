#import <YandexMapKitDirections/YMKDrivingRoute.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/directions/driving/route.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKDrivingRoute ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::directions::driving::Route>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::directions::driving::Route>)nativeRoute;
- (std::shared_ptr<::yandex::maps::mapkit::directions::driving::Route>)native;

@end
