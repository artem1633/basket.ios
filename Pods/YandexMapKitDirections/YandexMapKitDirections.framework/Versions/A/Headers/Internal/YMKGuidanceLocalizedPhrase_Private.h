#import <YandexMapKitDirections/YMKGuidanceLocalizedPhrase.h>

#import <yandex/maps/mapkit/directions/guidance/speaker.h>

#import <memory>

@interface YMKGuidanceLocalizedPhrase ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::directions::guidance::LocalizedPhrase>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::directions::guidance::LocalizedPhrase>)nativeLocalizedPhrase;
- (std::shared_ptr<::yandex::maps::mapkit::directions::guidance::LocalizedPhrase>)native;

@end
