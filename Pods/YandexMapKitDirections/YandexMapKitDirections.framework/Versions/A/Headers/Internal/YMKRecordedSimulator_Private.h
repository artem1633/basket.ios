#import <YandexMapKitDirections/YMKRecordedSimulator.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/directions/guidance/recorded_simulator.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKRecordedSimulator ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::directions::guidance::RecordedSimulator>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::directions::guidance::RecordedSimulator>)nativeRecordedSimulator;

@end
