#import <YandexMapKitDirections/YMKDrivingUturnMetadata.h>

#import <yandex/maps/mapkit/directions/driving/annotation.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::directions::driving::UturnMetadata, YMKDrivingUturnMetadata, void> {
    static ::yandex::maps::mapkit::directions::driving::UturnMetadata from(
        YMKDrivingUturnMetadata* platformUturnMetadata);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::directions::driving::UturnMetadata, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKDrivingUturnMetadata*>::value>::type> {
    static ::yandex::maps::mapkit::directions::driving::UturnMetadata from(
        PlatformType platformUturnMetadata)
    {
        return ToNative<::yandex::maps::mapkit::directions::driving::UturnMetadata, YMKDrivingUturnMetadata>::from(
            platformUturnMetadata);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::directions::driving::UturnMetadata> {
    static YMKDrivingUturnMetadata* from(
        const ::yandex::maps::mapkit::directions::driving::UturnMetadata& uturnMetadata);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
