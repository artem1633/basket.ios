#import <YandexMapKitDirections/YMKSpeedingPolicy.h>

#import <yandex/maps/mapkit/directions/guidance/speeding_policy.h>

#import <memory>

@interface YMKSpeedingPolicy ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::directions::guidance::SpeedingPolicy>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::directions::guidance::SpeedingPolicy>)nativeSpeedingPolicy;
- (std::shared_ptr<::yandex::maps::mapkit::directions::guidance::SpeedingPolicy>)native;

@end
