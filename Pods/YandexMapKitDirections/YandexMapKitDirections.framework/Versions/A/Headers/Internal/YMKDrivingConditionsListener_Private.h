#import <YandexMapKitDirections/YMKDrivingConditionsListener.h>

#import <yandex/maps/mapkit/directions/driving/route.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace directions {
namespace driving {
namespace ios {

class ConditionsListenerBinding : public ::yandex::maps::mapkit::directions::driving::ConditionsListener {
public:
    explicit ConditionsListenerBinding(
        id<YMKDrivingConditionsListener> platformListener);

    virtual void onConditionsUpdated() override;

    virtual void onConditionsOutdated() override;

    id<YMKDrivingConditionsListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKDrivingConditionsListener> platformListener_;
};

} // namespace ios
} // namespace driving
} // namespace directions
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::directions::driving::ConditionsListener>, id<YMKDrivingConditionsListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::directions::driving::ConditionsListener> from(
        id<YMKDrivingConditionsListener> platformConditionsListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::directions::driving::ConditionsListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::directions::driving::ConditionsListener> from(
        PlatformType platformConditionsListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::directions::driving::ConditionsListener>, id<YMKDrivingConditionsListener>>::from(
            platformConditionsListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::directions::driving::ConditionsListener>> {
    static id<YMKDrivingConditionsListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::directions::driving::ConditionsListener>& nativeConditionsListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
