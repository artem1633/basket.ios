#import <YandexMapKitDirections/YMKDrivingLeaveRoundaboutMetadata.h>

#import <yandex/maps/mapkit/directions/driving/annotation.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::directions::driving::LeaveRoundaboutMetadata, YMKDrivingLeaveRoundaboutMetadata, void> {
    static ::yandex::maps::mapkit::directions::driving::LeaveRoundaboutMetadata from(
        YMKDrivingLeaveRoundaboutMetadata* platformLeaveRoundaboutMetadata);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::directions::driving::LeaveRoundaboutMetadata, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKDrivingLeaveRoundaboutMetadata*>::value>::type> {
    static ::yandex::maps::mapkit::directions::driving::LeaveRoundaboutMetadata from(
        PlatformType platformLeaveRoundaboutMetadata)
    {
        return ToNative<::yandex::maps::mapkit::directions::driving::LeaveRoundaboutMetadata, YMKDrivingLeaveRoundaboutMetadata>::from(
            platformLeaveRoundaboutMetadata);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::directions::driving::LeaveRoundaboutMetadata> {
    static YMKDrivingLeaveRoundaboutMetadata* from(
        const ::yandex::maps::mapkit::directions::driving::LeaveRoundaboutMetadata& leaveRoundaboutMetadata);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
