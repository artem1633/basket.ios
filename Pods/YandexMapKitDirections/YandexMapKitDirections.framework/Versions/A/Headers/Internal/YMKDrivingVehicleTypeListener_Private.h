#import <YandexMapKitDirections/YMKDrivingVehicleTypeListener.h>

#import <yandex/maps/mapkit/directions/driving/driving_router.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace directions {
namespace driving {
namespace ios {

class VehicleTypeListenerBinding : public ::yandex::maps::mapkit::directions::driving::VehicleTypeListener {
public:
    explicit VehicleTypeListenerBinding(
        id<YMKDrivingVehicleTypeListener> platformListener);

    virtual void onVehicleTypeUpdated() override;

    id<YMKDrivingVehicleTypeListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKDrivingVehicleTypeListener> platformListener_;
};

} // namespace ios
} // namespace driving
} // namespace directions
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::directions::driving::VehicleTypeListener>, id<YMKDrivingVehicleTypeListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::directions::driving::VehicleTypeListener> from(
        id<YMKDrivingVehicleTypeListener> platformVehicleTypeListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::directions::driving::VehicleTypeListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::directions::driving::VehicleTypeListener> from(
        PlatformType platformVehicleTypeListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::directions::driving::VehicleTypeListener>, id<YMKDrivingVehicleTypeListener>>::from(
            platformVehicleTypeListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::directions::driving::VehicleTypeListener>> {
    static id<YMKDrivingVehicleTypeListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::directions::driving::VehicleTypeListener>& nativeVehicleTypeListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
