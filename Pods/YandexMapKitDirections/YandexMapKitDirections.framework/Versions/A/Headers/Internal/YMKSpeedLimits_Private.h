#import <YandexMapKitDirections/YMKSpeedLimits.h>

#import <yandex/maps/mapkit/directions/guidance/speeding_policy.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::directions::guidance::SpeedLimits, YMKSpeedLimits, void> {
    static ::yandex::maps::mapkit::directions::guidance::SpeedLimits from(
        YMKSpeedLimits* platformSpeedLimits);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::directions::guidance::SpeedLimits, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSpeedLimits*>::value>::type> {
    static ::yandex::maps::mapkit::directions::guidance::SpeedLimits from(
        PlatformType platformSpeedLimits)
    {
        return ToNative<::yandex::maps::mapkit::directions::guidance::SpeedLimits, YMKSpeedLimits>::from(
            platformSpeedLimits);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::directions::guidance::SpeedLimits> {
    static YMKSpeedLimits* from(
        const ::yandex::maps::mapkit::directions::guidance::SpeedLimits& speedLimits);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
