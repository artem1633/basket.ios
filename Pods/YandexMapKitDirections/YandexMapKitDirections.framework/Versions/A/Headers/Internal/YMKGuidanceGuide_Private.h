#import <YandexMapKitDirections/YMKGuidanceGuide.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/directions/guidance/guide.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKGuidanceGuide ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::directions::guidance::Guide>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::directions::guidance::Guide>)nativeGuide;
- (std::shared_ptr<::yandex::maps::mapkit::directions::guidance::Guide>)native;

@end
