#import <YandexMapKitDirections/YMKDrivingRouteSerializer.h>

#import <yandex/maps/mapkit/directions/driving/driving_router.h>

#import <memory>

@interface YMKDrivingRouteSerializer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::directions::driving::RouteSerializer>)native;

- (::yandex::maps::mapkit::directions::driving::RouteSerializer *)nativeRouteSerializer;

@end
