#import <YandexMapKitDirections/YMKDrivingSummarySession.h>

#import <yandex/maps/mapkit/directions/driving/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace directions {
namespace driving {
namespace ios {

SummarySession::OnDrivingSummaries onDrivingSummaries(
    YMKDrivingSummarySessionSummaryHandler handler);
SummarySession::OnDrivingSummariesError onDrivingSummariesError(
    YMKDrivingSummarySessionSummaryHandler handler);

} // namespace ios
} // namespace driving
} // namespace directions
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKDrivingSummarySession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::directions::driving::SummarySession>)native;

- (::yandex::maps::mapkit::directions::driving::SummarySession *)nativeSummarySession;

@end
