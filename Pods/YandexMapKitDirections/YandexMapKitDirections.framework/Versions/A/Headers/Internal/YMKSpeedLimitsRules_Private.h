#import <YandexMapKitDirections/YMKSpeedLimitsRules.h>

#import <yandex/maps/mapkit/directions/guidance/speeding_policy.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::directions::guidance::SpeedLimitsRules, YMKSpeedLimitsRules, void> {
    static ::yandex::maps::mapkit::directions::guidance::SpeedLimitsRules from(
        YMKSpeedLimitsRules* platformSpeedLimitsRules);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::directions::guidance::SpeedLimitsRules, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSpeedLimitsRules*>::value>::type> {
    static ::yandex::maps::mapkit::directions::guidance::SpeedLimitsRules from(
        PlatformType platformSpeedLimitsRules)
    {
        return ToNative<::yandex::maps::mapkit::directions::guidance::SpeedLimitsRules, YMKSpeedLimitsRules>::from(
            platformSpeedLimitsRules);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::directions::guidance::SpeedLimitsRules> {
    static YMKSpeedLimitsRules* from(
        const ::yandex::maps::mapkit::directions::guidance::SpeedLimitsRules& speedLimitsRules);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
