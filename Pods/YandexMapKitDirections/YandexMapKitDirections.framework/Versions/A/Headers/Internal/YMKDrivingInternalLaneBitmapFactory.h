#import <UIKit/UIKit.h>

#import <YandexMapKitDirections/YMKDrivingLane.h>

@interface YMKDrivingInternalLaneBitmapFactory : NSObject

+ (UIImage *)createLaneBitmapWithLaneSigns:(NSArray *)laneSigns;

@end
