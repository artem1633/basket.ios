#import <YandexMapKit/YMKLocalizedValue.h>

/// @cond EXCLUDE
@interface YMKSpeedLimits : NSObject

@property (nonatomic, readonly, nonnull) YMKLocalizedValue *urban;

@property (nonatomic, readonly, nonnull) YMKLocalizedValue *rural;

@property (nonatomic, readonly, nonnull) YMKLocalizedValue *expressway;


+ (nonnull YMKSpeedLimits *)speedLimitsWithUrban:(nonnull YMKLocalizedValue *)urban
                                           rural:(nonnull YMKLocalizedValue *)rural
                                      expressway:(nonnull YMKLocalizedValue *)expressway;


@end
/// @endcond

