#import <YandexMapKitDirections/YMKDrivingSummary.h>

#import <YandexRuntime/YRTPlatformBinding.h>

typedef void(^YMKDrivingSummarySessionSummaryHandler)(
    NSArray<YMKDrivingSummary *> *summaries,
    NSError *error);

/**
 * Driving session summary.
 */
@interface YMKDrivingSummarySession : YRTPlatformBinding

/**
 * Cancels route summary generation.
 */
- (void)cancel;


/**
 * Tries to generate a driving session summary again.
 */
- (void)retryWithSummaryHandler:(nonnull YMKDrivingSummarySessionSummaryHandler)summaryHandler;


@end

