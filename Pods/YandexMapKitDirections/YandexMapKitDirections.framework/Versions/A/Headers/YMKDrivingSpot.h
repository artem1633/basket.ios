#import <YandexMapKit/YMKPolylinePosition.h>

/**
 * The type of the spot on the route.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingSpotType) {

    /**
     * Unknown spot.
     */
    YMKDrivingSpotTypeUnknown,

    /**
     * A spot that restricts access.
     */
    YMKDrivingSpotTypeAccessPassRestriction
};


/**
 * A spot object.
 */
@interface YMKDrivingSpot : NSObject

/**
 * The type of the spot.
 */
@property (nonatomic, readonly) YMKDrivingSpotType type;

/**
 * The position of the spot.
 */
@property (nonatomic, readonly, nonnull) YMKPolylinePosition *position;


+ (nonnull YMKDrivingSpot *)spotWithType:( YMKDrivingSpotType)type
                                position:(nonnull YMKPolylinePosition *)position;


@end

